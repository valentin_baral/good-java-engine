package good.engine.output;

import good.engine.output.audio.gd_output_AudioLine;
import good.engine.output.render.gd_output_Renderer;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by valentinbaral on 02/11/2015.
 */
@Singleton
public class gd_Output {

    private gd_output_Renderer renderer;
    private gd_output_AudioLine audioLine;

    public gd_Output(){

        try {
            renderer = new gd_output_Renderer();
        }catch (Exception e){
            e.printStackTrace();
        }

        audioLine =  new gd_output_AudioLine();

    }

    public gd_output_Renderer getRenderer() {
        return renderer;
    }

    public gd_output_AudioLine getAudioLine() {
        return audioLine;
    }

}

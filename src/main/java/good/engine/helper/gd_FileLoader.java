package good.engine.helper;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by valentinbaral on 24/05/2016.
 */
public class gd_FileLoader {

    // can only read text documents
    public static ArrayList<String> read(String path){

        ArrayList<String> lines = new ArrayList<String>();

        // This will reference one line at a time
        String nextLine = null;

        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(path);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while((nextLine = bufferedReader.readLine()) != null) {

                // this should always be new instance?
                String line = nextLine;

                lines.add(line);

            }

            // Always close files.
            bufferedReader.close();

        }
        catch(FileNotFoundException ex) {
            System.out.println("Unable to open file: " + path);

        }
        catch(IOException ex) {
            System.out.println("Error reading file: " + path);
            // Or we could just do this:
            // ex.printStackTrace();
        }
        catch(java.lang.IndexOutOfBoundsException ex){
            System.out.println("Invalid file: " + path);
        }

        return lines;

    }

    public static void write(String path, ArrayList<String> lines){

        try {
            // Assume default encoding.
            FileWriter fileWriter = new FileWriter(path);

            // Always wrap FileWriter in BufferedWriter.
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for(int l = 0;l<lines.size();l++){

                bufferedWriter.write(lines.get(l));
                bufferedWriter.newLine();

            }

            // Always close files.
            bufferedWriter.close();
        }
        catch(IOException ex) {
            System.out.println( "Error writing to file: " + path);
            // Or we could just do this:
            // ex.printStackTrace();
        }

    }

}

package tests;

import com.badlogic.gdx.math.Vector3;
import good.math.path.Path;
import good.math.path.Vector3Interpolator;

import static org.junit.jupiter.api.Assertions.*;

class PositionInterpolatorTest {

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void getAtTime() {
        Path path = new Vector3Interpolator(new Vector3[]{new Vector3(0,0,0), new Vector3(1,1,1)});
        assertEquals(new Vector3(0 ,0,0), path.getAtTime(0), "Position at 0 failed");
        assertEquals(new Vector3(1,1,1), path.getAtTime(1), "Position at 1 failed");
        assertEquals(new Vector3(0.5f,0.5f,0.5f), path.getAtTime(0.5f), "Position at 0.5 failed");
    }

    @org.junit.jupiter.api.Test
    void getAtTimeHarder() {
        Vector3Interpolator path = new Vector3Interpolator(new Vector3[]{new Vector3(-1,-1,0),new Vector3(-1,1,0), new Vector3(1,1,0), new Vector3(1,-1,0)});
        //assertEquals(new Vector3(-1 ,-1,0), path.getAtTime(0), "Position at 0 failed");
        //assertEquals(new Vector3(1,-1,0), path.getAtTime(1), "Position at 1 failed");
        final Vector3 atTime = path.getAtTime(0.5f);
        assertEquals(0, new Vector3(0,1,0).sub(atTime).x, 0.1, "Position at 0.5 failed");
    }
}

package good.engine.moduleManager.modules.SoundManager;

/**
 * Created by valentinbaral on 24/11/2017.
 */
public class sm_Sound {

    protected String name = "map";
    private final short[] audioData;
    private boolean loop = false;
    private boolean playing = false;
    private long didStart = 0;

    public sm_Sound(String name, short[] audioData) {
        this.name = name;
        this.audioData = audioData;
    }

    public String getName() {
        return name;
    }

    public short[] getAudioData() {
        return audioData;
    }

    public boolean isLoop() {
        return loop;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public boolean isPlaying() {
        return playing;
    }

    void setPlaying(boolean playing) {
        this.playing = playing;
    }

    public long getDidStart() {
        return didStart;
    }

    public void setDidStart(long didStart) {
        this.didStart = didStart;
    }
}

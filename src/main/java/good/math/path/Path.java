package good.math.path;

import com.badlogic.gdx.math.Vector3;

public abstract class Path<T> {
    public abstract T getAtTime(float i);
}


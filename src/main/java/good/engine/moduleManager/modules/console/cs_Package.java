package good.engine.moduleManager.modules.console;

import java.util.ArrayList;

//import com.neet.*;

/**
 * Created by valentinbaral on 24/10/2015.
 */
public class cs_Package {

    private ArrayList<cs_Command> commands = new ArrayList<cs_Command>();

    private String name;

    private boolean core = false;

    private Object reference;

    private String type;

    public cs_Package(String name, Object reference, String type){

        this.name = name;

        this.type = type;
        this.reference = reference;

    }

    public void addCommand(cs_Command command){
        command.setPackage(this);
        commands.add(command);
    }

    public ArrayList<cs_Command> getCommands() {
        return commands;
    }

    public String getName() {
        return name;
    }

    public boolean isCore() {
        return core;
    }

    public Object getReference() {
        return reference;
    }

    public String getType() {
        return type;
    }

    public void setCore(boolean core) {
        this.core = core;
    }

}

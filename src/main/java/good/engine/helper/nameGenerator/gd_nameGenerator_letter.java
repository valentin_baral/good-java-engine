package good.engine.helper.nameGenerator;

/**
 * Created by valentinbaral on 22/12/2016.
 */
public class gd_nameGenerator_letter {

    private String letter;
    private float baseProbability = 1f;
    private float firstLetterProbability = 1f;
    private float lastLetterProbability = 1f;
    private float connectingLetterProbability = 1f;

    public gd_nameGenerator_letter(String letter){
        this.letter = letter;
    }

    public gd_nameGenerator_letter(String letter, float baseProbability){
        this.letter = letter;
        this.baseProbability = baseProbability;
    }

    public gd_nameGenerator_letter(String letter, float baseProbability, float firstLetterProbability){
        this.letter = letter;
        this.baseProbability = baseProbability;
        this.firstLetterProbability = firstLetterProbability;
    }

    public gd_nameGenerator_letter(String letter, float baseProbability, float firstLetterProbability, float lastLetterProbability){
        this.letter = letter;
        this.baseProbability = baseProbability;
        this.firstLetterProbability = firstLetterProbability;
        this.lastLetterProbability = lastLetterProbability;
    }

    public gd_nameGenerator_letter(String letter, float baseProbability, float firstLetterProbability, float lastLetterProbability, float connectingLetterProbability){
        this.letter = letter;
        this.baseProbability = baseProbability;
        this.firstLetterProbability = firstLetterProbability;
        this.lastLetterProbability = lastLetterProbability;
        this.connectingLetterProbability = connectingLetterProbability;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public float getBaseProbability() {
        return baseProbability;
    }

    public void setBaseProbability(float baseProbability) {
        this.baseProbability = baseProbability;
    }

    public float getFirstLetterProbability() {
        return firstLetterProbability;
    }

    public void setFirstLetterProbability(float firstLetterProbability) {
        this.firstLetterProbability = firstLetterProbability;
    }

    public float getLastLetterProbability() {
        return lastLetterProbability;
    }

    public void setLastLetterProbability(float lastLetterProbability) {
        this.lastLetterProbability = lastLetterProbability;
    }

    public float getConnectingLetterProbability() {
        return connectingLetterProbability;
    }

    public void setConnectingLetterProbability(float connectingLetterProbability) {
        this.connectingLetterProbability = connectingLetterProbability;
    }
}

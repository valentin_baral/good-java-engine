package good.engine.moduleManager.modules.particleEffects;

import good.engine.entity.graphics.gd_gfx_Model;
import good.engine.helper.gd_Math;
import good.engine.main.gd_Build;
import good.engine.main.gd_Config;
import good.engine.moduleManager.gd_Module;
import good.engine.output.render.gd_output_Renderer;

import java.util.*;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Random;
import java.util.Vector;

import static org.lwjgl.opengl.GL11.*;


/**
 * Created by Valentin on 14/07/2015.
 */

@Singleton
public class pf_ParticleEffects extends gd_Module {

    private int count = 0;

    public Vector<Float> positionX = new Vector<>();
    public Vector<Float> positionY = new Vector<>();
    public Vector<Float> positionZ = new Vector<>();
    public Vector<Float> sizes = new Vector<>();

    public ArrayList<gd_gfx_Model> models = new ArrayList<>();

    public Vector<pf_Effect> effect = new Vector<>();
    public Vector<Integer> directionX = new Vector<>();
    public Vector<Integer> directionY = new Vector<>();
    public Vector<Integer> directionZ = new Vector<>();
    public Vector<Float> dropTime = new Vector<>();

    @Inject
    public pf_ParticleEffects() {
        setName("particleEffects");
    }

    public void update(gd_Build engine){
        for(int i=0; i < count; i++) {
            updateParticle(i);
        }
    }

    public void draw(gd_output_Renderer renderer){

        glEnable(GL_COLOR_MATERIAL);

        for(int i=0; i<count; i++){
            drawParticle(i, renderer);
        }


    }

    private void updateParticle(int i){

        if(dropTime.get(i)>0){
            dropTime.set(i, dropTime.get(i)- gd_Config.hardDeltaTime());
        }

        if(dropTime.get(i)<=0){
            removeParticle(i);
            return;
        }

        effect.get(i).execute(this, i);

    }

    private void drawParticle(int i, gd_output_Renderer renderer){
        //GL11.glColor3f(0.4f,0.4f,0);
        glPointSize(sizes.get(i));
        GL11.glBegin(GL_POINTS);
        GL11.glColor3f(0.5f,0.5f,0.6f);
        glVertex3f(positionX.get(i), positionY.get(i), positionZ.get(i));
        glEnd();
    }


    private void removeParticle(int i){

        effect.remove(i);

        positionX.remove(i);
        positionY.remove(i);
        positionZ.remove(i);
        sizes.remove(i);

        models.remove(i);

        dropTime.remove(i);
        directionX.remove(i);
        directionY.remove(i);
        directionZ.remove(i);
        count--;

    }

    public void clean(){
        effect.clear();

        positionX.clear();
        positionY.clear();
        positionZ.clear();
        sizes.clear();

        models.clear();

        dropTime.clear();
        directionX.clear();
        directionY.clear();
        directionZ.clear();
        count = 0;
    }

    public void addParticleWithSize(float x, float y, float z, float size, gd_gfx_Model model, pf_Effect gfx, int amount, float time)
    {
        if(model == null || gfx == null) throw new NullPointerException();

        for(int i=0; i<amount; i++){

            effect.add(gfx);
            positionX.add(x);
            positionY.add(y);
            positionZ.add(z);
            sizes.add(size);
            dropTime.add(time);
            models.add(model);

            //directionX.add(gd_Math.random.nextBoolean() ? 1 : -1);
            //directionY.add(gd_Math.random.nextBoolean() ? 1 : -1);
            //directionZ.add(gd_Math.random.nextBoolean() ? 1 : -1);
            directionX.add(1);
            directionY.add(1);
            directionZ.add(1);

            count++;

        }
    }
    public void addParticle(float x, float y, float z, gd_gfx_Model model, pf_Effect gfx, int amount, float time){
        addParticleWithSize(x,y,z,1f,model,gfx,amount,time);
    }

}

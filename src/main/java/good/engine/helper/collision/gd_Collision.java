package good.engine.helper.collision;

/**
 * Created by valentinbaral on 10/07/15.
 */

import good.engine.entity.hitbox.gd_Hitbox;
import good.engine.entity.objects.gd_Object;
import good.engine.helper.gd_Math;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.Arrays;

public class gd_Collision {

    /*

    Provisional collision detection

     */

    public static boolean debug = false;

    private static Vector3f tempVector = new Vector3f(0,0,0);

    public gd_Collision(){

    }

    public static gd_hitObjectPair ray(Vector3f from, Vector3f direction, ArrayList<gd_Object> objects, float distanceFrom, float distanceTo, gd_Object[] objectsToIgnore, Class[] classesToIgnore) {

        gd_Object hit = null;

        if(objectsToIgnore == null) objectsToIgnore = new gd_Object[]{};
        if(classesToIgnore == null) classesToIgnore = new Class[]{};

        float multi = 1;
        float distance = 0;

        float x = from.x;
        float y = from.y;
        float z = from.z;

        while (distance < distanceTo) {

            x += (multi * (float) Math.sin(Math.toRadians(direction.y)));
            y += (multi * (float) -Math.tan(Math.toRadians(direction.x)));
            z += (multi * (float) -Math.cos(Math.toRadians(direction.y)));

            // check

            tempVector.x = x;
            tempVector.y = y;
            tempVector.z = z;

            for (int o = 0; o < objects.size(); o++) {

                gd_Object object = objects.get(o);

                if(!object.isActive() || !object.isActive2()) continue;
                //if(!object.isPhysical()) continue;
                if(!object.isSolid()) continue;

                boolean hasHit = checkForPoint(tempVector, object);

                if(hasHit) {

                    boolean lock = false;

                    for(int c=0; c<classesToIgnore.length; c++){
                        if(object.getClass() == classesToIgnore[c]){
                            lock = true;
                        }
                    }

                    for(int c=0; c<objectsToIgnore.length; c++){
                        if(object == objectsToIgnore[c]){
                            lock = true;
                        }
                    }

                    if(lock != true) {
                        hit = object;
                        break;
                    }

                }

            }

            distance = gd_Math.calcDistance3D(from.x, from.y, from.z, x, y, z);

            if(hit != null){
                gd_hitObjectPair out = new gd_hitObjectPair();
                out.set(hit,tempVector);
                return out;
            }

        }

        return null;

    }

    public static boolean checkFor(gd_Object check, gd_Object checkFor){

        Vector3f checkPosition = check.getPosition();
        Vector3f checkPositionNext = check.getNextPosition();

        for(int h=0; h<check.getHitboxes().size(); h++) {

            gd_Hitbox checkHitbox = check.getHitboxes().get(h);

            Vector3f checkHitboxSize = checkHitbox.getSize();
            Vector3f checkHitboxPosition = checkHitbox.getPosition();

            if (debug)
                System.out.println("CHECK HITBOX #" + h);


            gd_Object object = checkFor;

            Vector3f objectPosition = object.getPosition();
            Vector3f objectPositionNext = object.getNextPosition();

            for (int h2 = 0; h2 < object.getHitboxes().size(); h2++) {

                gd_Hitbox objectHitbox = object.getHitboxes().get(h2);

                Vector3f objectHitboxSize = objectHitbox.getSize();
                Vector3f objectHitboxPosition = objectHitbox.getPosition();

                if (((checkPositionNext.x + checkHitboxPosition.x + checkHitboxSize.x / 2 > objectPosition.x + objectHitboxPosition.x - objectHitboxSize.x / 2) &&
                        (checkPositionNext.x + checkHitboxPosition.x - checkHitboxSize.x / 2 < objectPosition.x + objectHitboxPosition.x + objectHitboxSize.x / 2))) {

                    if (((checkPositionNext.y + checkHitboxPosition.y + checkHitboxSize.y / 2 > objectPosition.y + objectHitboxPosition.y - objectHitboxSize.y / 2) &&
                            (checkPositionNext.y + checkHitboxPosition.y - checkHitboxSize.y / 2 < objectPosition.y + objectHitboxPosition.y + objectHitboxSize.y / 2))) {

                        if (((checkPositionNext.z + checkHitboxPosition.z + checkHitboxSize.z / 2 > objectPosition.z + objectHitboxPosition.z - objectHitboxSize.z / 2) &&
                                (checkPositionNext.z + checkHitboxPosition.z - checkHitboxSize.z / 2 < objectPosition.z + objectHitboxPosition.z + objectHitboxSize.z / 2))) {

                            return true;

                        }
                    }
                }
            }
        }


        return false;
    }

    public static boolean checkForPoint(Vector3f check, gd_Object checkFor) {

        Vector3f checkPosition = check;

        gd_Object object = checkFor;

        Vector3f objectPosition = object.getPosition();
        Vector3f objectPositionNext = object.getNextPosition();

        for (int h2 = 0; h2 < object.getHitboxes().size(); h2++) {

            gd_Hitbox objectHitbox = object.getHitboxes().get(h2);

            Vector3f objectHitboxSize = objectHitbox.getSize();
            Vector3f objectHitboxPosition = objectHitbox.getPosition();

            if (((checkPosition.x > objectPosition.x + objectHitboxPosition.x - objectHitboxSize.x / 2) &&
                    (checkPosition.x < objectPosition.x + objectHitboxPosition.x + objectHitboxSize.x / 2))) {

                if (((checkPosition.y > objectPosition.y + objectHitboxPosition.y - objectHitboxSize.y / 2) &&
                        (checkPosition.y < objectPosition.y + objectHitboxPosition.y + objectHitboxSize.y / 2))) {

                    if (((checkPosition.z > objectPosition.z + objectHitboxPosition.z - objectHitboxSize.z / 2) &&
                            (checkPosition.z < objectPosition.z + objectHitboxPosition.z + objectHitboxSize.z / 2))) {

                        return true;

                    }
                }
            }
        }


        return false;

    }

    // make better hitdetection with cool algorythms to check any for with any rotation against any other forms

    public static void check(gd_Object check, ArrayList<gd_Object> objects){

        if(!check.isActive() || !check.isActive2()) return;
        if(!check.isPhysical()) return;
        if(!check.isSolid()) return;

        if(debug)
            System.out.println("PHYSICAL COLLISION DETECTION");

        boolean COLL_LEFT = false;
        boolean COLL_TOP = false;
        boolean COLL_FRONT = false;
        boolean COLL_RIGHT = false;
        boolean COLL_BOTTOM = false;
        boolean COLL_BACK = false;

        Vector3f checkPosition = check.getPosition();
        Vector3f checkPositionNext = check.getNextPosition();

        for(int h=0; h<check.getHitboxes().size(); h++) {

            gd_Hitbox checkHitbox = check.getHitboxes().get(h);

            Vector3f checkHitboxSize = checkHitbox.getSize();
            Vector3f checkHitboxPosition = checkHitbox.getPosition();

            if(debug)
                System.out.println("CHECK HITBOX #"+h);


            for (int o = 0; o < objects.size(); o++) {

                gd_Object object = objects.get(o);

                if(!object.isActive() || !objects.get(o).isActive2()) continue;
                if(!object.isSolid() && !object.isTrigger()) continue;

                if(object == check) continue;


                Vector3f objectPosition = object.getPosition();
                Vector3f objectPositionNext = object.getNextPosition();

                for (int h2 = 0; h2 < object.getHitboxes().size(); h2++) {

                    gd_Hitbox objectHitbox = object.getHitboxes().get(h2);

                    Vector3f objectHitboxSize = objectHitbox.getSize();
                    Vector3f objectHitboxPosition = objectHitbox.getPosition();

                    if (((checkPositionNext.x + checkHitboxPosition.x + checkHitboxSize.x/2 > objectPosition.x + objectHitboxPosition.x - objectHitboxSize.x/2) &&
                            (checkPositionNext.x + checkHitboxPosition.x - checkHitboxSize.x/2 < objectPosition.x + objectHitboxPosition.x + objectHitboxSize.x/2))) {

                        if (((checkPositionNext.y + checkHitboxPosition.y + checkHitboxSize.y/2 > objectPosition.y + objectHitboxPosition.y - objectHitboxSize.y/2) &&
                                (checkPositionNext.y + checkHitboxPosition.y - checkHitboxSize.y/2 < objectPosition.y + objectHitboxPosition.y + objectHitboxSize.y/2))) {

                            if (((checkPositionNext.z + checkHitboxPosition.z + checkHitboxSize.z/2 > objectPosition.z + objectHitboxPosition.z - objectHitboxSize.z/2) &&
                                    (checkPositionNext.z + checkHitboxPosition.z - checkHitboxSize.z/2 < objectPosition.z + objectHitboxPosition.z + objectHitboxSize.z/2))) {

                                if(debug)
                                    System.out.println("OBJECT HITBOX HIT #"+h2);

                                if (object.isTrigger()) {
                                    check.onCollision(object);
                                    object.onCollision(check);

                                    continue;
                                }

                                boolean MOD_COLL_LEFT = false;
                                boolean MOD_COLL_TOP = false;
                                boolean MOD_COLL_FRONT = false;
                                boolean MOD_COLL_RIGHT = false;
                                boolean MOD_COLL_BOTTOM = false;
                                boolean MOD_COLL_BACK = false;

                                float leftOverlap = (objectPosition.x + objectHitboxPosition.x + objectHitboxSize.x/2) - (checkPositionNext.x + checkHitboxPosition.x - checkHitboxSize.x/2);
                                float rightOverlap = (checkPositionNext.x + checkHitboxPosition.x + checkHitboxSize.x/2) - (objectPosition.x + objectHitboxPosition.x - objectHitboxSize.x/2);

                                float bottomOverlap = (objectPosition.y + objectHitboxPosition.y + objectHitboxSize.y/2) - (checkPositionNext.y + checkHitboxPosition.y - checkHitboxSize.y/2);
                                float topOverlap = (checkPositionNext.y + checkHitboxPosition.y + checkHitboxSize.y/2) - (objectPosition.y + objectHitboxPosition.y - objectHitboxSize.y/2);

                                float backOverlap = (objectPosition.z + objectHitboxPosition.z + objectHitboxSize.z/2) - (checkPositionNext.z + checkHitboxPosition.z - checkHitboxSize.z/2);
                                float frontOverlap = (checkPositionNext.z + checkHitboxPosition.z + checkHitboxSize.z/2) - (objectPosition.z + objectHitboxPosition.z - objectHitboxSize.z/2);

                                // igonre below or smth
                                float rad = 5f;

                                if( bottomOverlap < rad ){

                                    if(leftOverlap < rad){
                                        MOD_COLL_LEFT = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_LEFT");

                                    }else if(rightOverlap < rad){
                                        MOD_COLL_RIGHT = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_RIGHT");

                                    }

                                    if(backOverlap < rad){
                                        MOD_COLL_BACK = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_BACK");

                                    }else if(frontOverlap < rad){
                                        MOD_COLL_FRONT = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_FRONT");

                                    }

                                } else if( topOverlap < rad ){

                                    if(leftOverlap < rad){
                                        MOD_COLL_LEFT = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_LEFT");

                                    }else if(rightOverlap < rad){
                                        MOD_COLL_RIGHT = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_RIGHT");

                                    }

                                    if(backOverlap < rad){
                                        MOD_COLL_BACK = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_BACK");

                                    }else if(frontOverlap < rad){
                                        MOD_COLL_FRONT = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_FRONT");

                                    }

                                }


                                if(leftOverlap < rad){
                                    if(backOverlap < rad){
                                        MOD_COLL_BACK = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_BACK");

                                    }else if(frontOverlap < rad){
                                        MOD_COLL_FRONT = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_FRONT");

                                    }
                                } else if(rightOverlap < rad){
                                    if(backOverlap < rad){
                                        MOD_COLL_BACK = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_BACK");

                                    }else if(frontOverlap < rad){
                                        MOD_COLL_FRONT = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_FRONT");

                                    }
                                }

                                if(backOverlap < rad){
                                    if(leftOverlap < rad){
                                        MOD_COLL_LEFT = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_LEFT");

                                    }else if(rightOverlap < rad){
                                        MOD_COLL_RIGHT = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_RIGHT");

                                    }
                                } else if(frontOverlap < rad){
                                    if(leftOverlap < rad){
                                        MOD_COLL_LEFT = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_LEFT");

                                    }else if(rightOverlap < rad){
                                        MOD_COLL_RIGHT = true;

                                        if(debug)
                                            System.out.println("MOD_COLL_RIGHT");

                                    }
                                }

                                //float values[] = {leftOverlap, rightOverlap, bottomOverlap, topOverlap, backOverlap, frontOverlap};
                                float values[] = {leftOverlap, rightOverlap, bottomOverlap, topOverlap};

                                int index[] = new int[6];

                                Arrays.sort(values);

                                for(int v=0; v<values.length; v++){

                                    float val = values[v];

                                    if(debug)
                                        System.out.println(val);

                                }

                                for(int v=0; v<values.length; v++){

                                    //System.out.println("TRY = "+v);

                                    float val = values[v];

                                    if(val < 0) continue;

                                    if(val == leftOverlap){
                                        //coll left

                                        if(objectPosition.x + objectHitboxPosition.x > checkPositionNext.x + checkHitboxPosition.x) break;

                                        if(MOD_COLL_LEFT) continue;

                                        checkPositionNext.x = (objectPosition.x + objectHitboxPosition.x + objectHitboxSize.x/2) + (checkHitboxPosition.x + checkHitboxSize.x/2);

                                        COLL_LEFT = true;

                                        if(debug)
                                            System.out.println("COLL_LEFT = "+leftOverlap);

                                    }else if(val == rightOverlap){
                                        //coll right

                                        if(objectPosition.x + objectHitboxPosition.x < checkPositionNext.x + checkHitboxPosition.x) break;

                                        if(MOD_COLL_RIGHT) continue;

                                        checkPositionNext.x = (objectPosition.x + objectHitboxPosition.x - objectHitboxSize.x/2) + (checkHitboxPosition.x - checkHitboxSize.x/2);

                                        COLL_RIGHT = true;

                                        if(debug)
                                            System.out.println("COLL_RIGHT = "+rightOverlap);

                                    }else if(val == bottomOverlap){
                                        //coll bottom

                                        if(objectPosition.y + objectHitboxPosition.y > checkPositionNext.y + checkHitboxPosition.y) break;

                                        checkPositionNext.y = (objectPosition.y + objectHitboxPosition.y + objectHitboxSize.y/2) + (checkHitboxPosition.y + checkHitboxSize.y/2);

                                        COLL_BOTTOM = true;

                                        if(debug)
                                            System.out.println("COLL_BOTTOM = "+bottomOverlap);

                                    }else if(val == topOverlap){
                                        //coll top

                                        if(objectPosition.y + objectHitboxPosition.y < checkPositionNext.y + checkHitboxPosition.y) break;

                                        checkPositionNext.y = (objectPosition.y + objectHitboxPosition.y - objectHitboxSize.y/2) + (checkHitboxPosition.y - checkHitboxSize.y/2);

                                        COLL_TOP = true;

                                        if(debug)
                                            System.out.println("COLL_TOP = "+topOverlap);

                                    }else if(val == backOverlap){
                                        //coll back

                                        if(objectPosition.z + objectHitboxPosition.z > checkPositionNext.z + checkHitboxPosition.z) break;

                                        if(MOD_COLL_BACK) continue;

                                        checkPositionNext.z = (objectPosition.z + objectHitboxPosition.z + objectHitboxSize.z/2) + (checkHitboxPosition.z + checkHitboxSize.z/2);

                                        COLL_BACK = true;

                                        if(debug)
                                            System.out.println("COLL_BACK = "+backOverlap);

                                    }else if(val == frontOverlap){
                                        //coll front

                                        if(objectPosition.z + objectHitboxPosition.z < checkPositionNext.z + checkHitboxPosition.z) break;

                                        if(MOD_COLL_FRONT) continue;

                                        checkPositionNext.z = (objectPosition.z + objectHitboxPosition.z - objectHitboxSize.z/2) + (checkHitboxPosition.z - checkHitboxSize.z/2);

                                        COLL_FRONT = true;

                                        if(debug)
                                            System.out.println("COLL_FRONT = "+frontOverlap);

                                    }

                                    break;

                                }

                                // some hitbox collides with some other hitbox
                                check.onCollision(object);
                                object.onCollision(check);
                                // might be called twice but inconsistant if not

                            }
                        }
                    }
                }
            }
        }

        check.setCollision(COLL_TOP, COLL_BOTTOM, COLL_LEFT, COLL_RIGHT, COLL_FRONT, COLL_BACK);

    }

    /**
     * Created by Valentin on 13/02/2016.
     */
    public static class gd_hitObjectPair {

        private gd_Object object = null;
        private Vector3f position = new Vector3f();

        public gd_hitObjectPair(){}

        public void set(gd_Object object, Vector3f position){
            this.object = object;
            this.position.set(position);
        }

        public gd_Object getObject() {
            return object;
        }

        public Vector3f getPosition() {
            return position;
        }
    }
}
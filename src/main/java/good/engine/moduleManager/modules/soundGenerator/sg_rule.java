package good.engine.moduleManager.modules.soundGenerator;

/**
 * Created by Valentin on 06/08/2015.
 */
public class sg_rule {

    //public enum relation {oneToTwo, oneToAny, anyToOne} no need

    public enum types {ONE_AFTER_TWO, CHANCE, NOT, NOT_UNTIL}

    // switch war in rule for continous

    private types type;
    private float chance = 1;
    private sg_channelType comp1;
    private sg_channelType comp2;
    private boolean triggered = false;

    public sg_rule(types type, sg_channelType comp1, sg_channelType comp2){
        this.type = type;
        this.comp1 = comp1;
        this.comp2 = comp2;
    }

    public types getType(){
        return type;
    }

    public sg_channelType getComp2() {
        return comp2;
    }

    public float getChance() {
        return chance;
    }

    public void setChance(float chance) {
        this.chance = chance;
    }

    public sg_channelType getComp1() {
        return comp1;
    }

    public boolean isTriggered() {
        return triggered;
    }

    public void setTriggered(boolean triggered) {
        this.triggered = triggered;
    }
}

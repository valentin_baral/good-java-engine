package good.engine.moduleManager;

import dagger.Module;
import dagger.Provides;
import good.engine.input.gd_Input;
import good.engine.main.gd_Build;
import good.engine.output.gd_Output;
import good.engine.output.render.gd_output_Renderer;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by valentinbaral on 03/11/2015.
 */
@Module
@Singleton
public class gd_ModuleManager {

    private ArrayList<gd_Module> modules = new ArrayList<gd_Module>();

    @Inject
    public gd_ModuleManager(){
        System.out.println("initialised modulemanager");
    }

    @Provides
    @Singleton
    public gd_Output getOutput()
    {
        return new gd_Output();
    }

    public void addModule(gd_Module module, int weight){
        module.setWeight(weight);
        modules.add(module);
    }

    public gd_Module getModuleForName(String name){

        for(int m=0; m<modules.size(); m++){
            if(modules.get(m).getName() == name){
                return modules.get(m);
            }
        }

        return null;

    }

    /*
    public <T> T getModuleForName(String name){

        for(int m=0; m<modules.size(); m++){

            gd_module mod = modules.get(m);

            if(modules.get(m).getName() == name){

                String className = mod.getClass().getName();

                return (className) mod;
            }
        }

        return null;

    }

    public <T> T cast(Class<T> theclass){

        try {
            return theclass.cast(this);
        } catch(ClassCastException e) {
            return null;
        }

    }
    */

    public void initModules(gd_Build engine){

        Collections.sort(modules, new gd_moduleComparator());

        for(int m = 0; m < modules.size(); m++){
            modules.get(m).init(engine);
        }
    }

    public void preUpdate(gd_Build engine){

        for(int m = 0; m < modules.size(); m++){
            modules.get(m).preUpdate(engine);
        }

    }

    public void update(gd_Build engine){

        for(int m = 0; m < modules.size(); m++){
            modules.get(m).update(engine);
        }

    }

    public void postUpdate(gd_Build engine){

        for(int m = 0; m < modules.size(); m++){
            modules.get(m).postUpdate(engine);
        }

    }

    public void preDraw(gd_output_Renderer renderer){

        for(int m = 0; m < modules.size(); m++){
            modules.get(m).preDraw(renderer);
        }

    }

    public void draw(gd_output_Renderer renderer){

        for(int m = 0; m < modules.size(); m++){
            modules.get(m).draw(renderer);
        }

    }

    public void postDraw(gd_output_Renderer renderer){

        for(int m = 0; m < modules.size(); m++){
            modules.get(m).postDraw(renderer);
        }

    }

    public void drawHud(gd_output_Renderer renderer){

        for(int m = 0; m < modules.size(); m++){
            modules.get(m).drawHud(renderer);
        }

    }

    public void takeInput(gd_Input input){
        for(int m=0; m<modules.size(); m++){
            modules.get(m).takeInput(input);
        }
    }

    public void takePolledInput(gd_Input input){
        for(int m=0; m<modules.size(); m++){
            modules.get(m).takePolledInput(input);
        }
    }

    /**
     * Created by valentinbaral on 03/11/2015.
     */

    public static class gd_moduleComparator implements Comparator<gd_Module> {
        @Override
        public int compare(gd_Module m1, gd_Module m2) {

            if(m1.getWeight() > m2.getWeight()){
                return -1;
            }else if(m1.getWeight() < m2.getWeight()){
                return 1;
            }

            return 0;

        }
    }
}

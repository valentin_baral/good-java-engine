package good.engine.entity.graphics;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import good.engine.main.gd_Config;
import good.math.gd_Math;
import org.lwjgl.util.Color;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;

/**
 * Created by valentinbaral on 15/11/2015.
 */
public class gd_gfx_Model {

    private ArrayList<gd_gfx_Shape> shapes = new ArrayList<gd_gfx_Shape>();

    private ArrayList<gd_gfx_Animation> animations = new ArrayList<gd_gfx_Animation>();

    private Vector3f position = new Vector3f(0,0,0);
    private Vector3f originalRotation = new Vector3f(0,0,0);
    private Vector3f rotation = new Vector3f(0,0,0);
    private Vector3f scale = new Vector3f(1,1,1);

    private gd_gfx_Animation activeAnimation = null;
    private float animationPoint = 0;

    public boolean hasFlatTexture = false;
    private boolean visible = true;

    public gd_gfx_Model(){

    }

    public ArrayList<gd_gfx_Shape> getShapes() {
        return shapes;
    }

    public void addShape(gd_gfx_Shape Shape){
        shapes.add(Shape);
    }

    public Vector3f getPosition() {
        return new Vector3f(position);
    }

    public void setPosition(Vector3f position) {
        System.out.println("setting position");
        this.position.set(position);
    }

    public ArrayList<gd_gfx_Animation> getAnimations() {
        return animations;
    }

    public gd_gfx_Animation getAnimationForName(String name){

        for(int a=0; a<animations.size(); a++){
            if(animations.get(a).getName() == name){
                return animations.get(a);
            }
        }

        return null;

    }

    public void playAnimation(String name){

        if (activeAnimation != null) stopAnimation();

        gd_gfx_Animation animation = getAnimationForName(name);

        //System.out.println("play animation "+name);

        if(animation == null) {
            //System.out.println("not found");
            return;
        }

        startAnimation(animation);

    }

    public void setTexture(gd_gfx_Texture texture){
        // sets texture for all shapes
        for(int s=0; s<shapes.size(); s++){
            shapes.get(s).setTexture(texture);
        }
    }

    public void setColor(Color color)
    {
        this.setColor(color.getRed(),color.getGreen(),color.getBlue());
    }
    public void setColor(int red, int green, int blue){
        // sets color for all shapes
        for(int s=0; s<shapes.size(); s++){
            shapes.get(s).setColor(red, green, blue);
        }
    }
    public void setScale(Vector3f scale)
    {
        this.scale.set(scale);
    }
    public void setScale(Vector3 scale)
    {
        this.scale.set(gd_Math.convertVector(scale));
    }

    public Vector3f getScale() {
        return scale;
    }
    public Vector3 getScale2() {
        return gd_Math.convertVector(getScale());
    }

    public void startAnimation(gd_gfx_Animation animation) {
        reset();
        animationPoint = 0;
        activeAnimation = animation;
    }

    private void stopAnimation() {
        animationPoint = 0;
        activeAnimation = null;
    }

    public void reset() {
        stopAnimation();
    }

    public void updateAnimations(){

        resetAnimations();

        //for(int a=0; a<animations.size(); a++) {

        if (activeAnimation != null) {

            gd_gfx_Animation animation = activeAnimation;

            //if(!animation.isActive()) continue;

            float timeLength = animation.getTimeLength();
            animationPoint = animationPoint + (1f / (float)gd_Config.updatesPerSecond);
            //animation.setTimePoint(timePoint);

            float animationPercentage = animationPoint/timeLength;

            if(animationPercentage > 1f){

                if(animation.isStayOnLast()) {
                    animationPercentage = 0.99f;
                } else {
                    stopAnimation();
                    if(animation.isLoop()){
                        startAnimation(animation);
                        animationPoint = 0 + (1f / (float)gd_Config.updatesPerSecond);
                        animationPercentage = animationPoint/timeLength;
                    }
                    else return;
                }

            }

            for(int g=0; g<animation.getAnimationGroups().size(); g++){

                gd_gfx_AnimationGroup animationGroup = animation.getAnimationGroups().get(g);

                if(animationPercentage < animationGroup.getStartPoint()) continue;

                // percentage length
                float animationGroupLength = animationGroup.getEndPoint()-animationGroup.getStartPoint();

                //?, if none is set this shouldn't effect anything
                float animationGroupPercentage = (animationPercentage-animationGroup.getStartPoint())/animationGroupLength;

                if(animationGroupPercentage <= 1f) {


                    Vector3f closestPrevious = null;
                    float lastPreviousPositionModifierPercentage = 2;
                    Vector3f closestNext = null;
                    float lastNextPositionModifierPercentage = 1;

                    int positionModifierCount = animationGroup.getPositionModifiers().size();

                    for(int p=0; p<positionModifierCount; p++){
                        float positionModifierPercentage = (float)p/(float)positionModifierCount;

                        //System.out.println("positionModifierPercentage "+positionModifierPercentage);

                        if(positionModifierPercentage > animationGroupPercentage){
                            if(closestNext == null){
                                closestNext = animationGroup.getPositionModifiers().get(p);
                                lastNextPositionModifierPercentage = positionModifierPercentage;
                            }
                            else if(positionModifierPercentage-animationGroupPercentage < lastNextPositionModifierPercentage-animationGroupPercentage){
                                closestNext = animationGroup.getPositionModifiers().get(p);
                                lastNextPositionModifierPercentage = positionModifierPercentage;
                            }
                        }else{
                            if(closestPrevious == null) {
                                closestPrevious = animationGroup.getPositionModifiers().get(p);
                                lastPreviousPositionModifierPercentage = positionModifierPercentage;
                            }
                            else if(animationGroupPercentage-positionModifierPercentage < animationGroupPercentage-lastPreviousPositionModifierPercentage){
                                closestPrevious = animationGroup.getPositionModifiers().get(p);
                                lastPreviousPositionModifierPercentage = positionModifierPercentage;
                            }
                        }
                    }

                    float percentageBetweenModifiers =
                            (animationGroupPercentage-lastPreviousPositionModifierPercentage)/
                            (lastNextPositionModifierPercentage-lastPreviousPositionModifierPercentage);



                    float newXMod = 0;
                    float newYMod = 0;
                    float newZMod = 0;

                    float previousX = 0;
                    float previousY = 0;
                    float previousZ = 0;

                    float nextX = 0;
                    float nextY = 0;
                    float nextZ = 0;

                    if(closestPrevious != null) {

                        previousX = closestPrevious.x;
                        previousY = closestPrevious.y;
                        previousZ = closestPrevious.z;

                    }else if(closestNext != null) {

                        previousX = closestNext.x;
                        previousY = closestNext.y;
                        previousZ = closestNext.z;

                    }

                    if(closestNext != null) {

                        nextX = closestNext.x;
                        nextY = closestNext.y;
                        nextZ = closestNext.z;

                    }else if(closestPrevious != null) {

                        nextX = closestPrevious.x;
                        nextY = closestPrevious.y;
                        nextZ = closestPrevious.z;

                    }

                    newXMod = previousX+((nextX-previousX)*percentageBetweenModifiers);
                    newYMod = previousY+((nextY-previousY)*percentageBetweenModifiers);
                    newZMod = previousZ+((nextZ-previousZ)*percentageBetweenModifiers);

                    for(int s=0; s<animationGroup.getShapes().size(); s++){

                        gd_gfx_Shape shape = animationGroup.getShapes().get(s);

                        shape.getPosition().x = shape.getOriginalPosition().x+newXMod;
                        shape.getPosition().y = shape.getOriginalPosition().y+newYMod;
                        shape.getPosition().z = shape.getOriginalPosition().z+newZMod;

                    }

                    Vector3f closestPreviousScale = null;
                    float lastPreviousScaleModifierPercentage = 2;
                    Vector3f closestNextScale = null;
                    float lastNextScaleModifierPercentage = 1;

                    int scaleModifierCount = animationGroup.getScaleModifiers().size();

                    for(int p=0; p<scaleModifierCount; p++){
                        float scaleModifierPercentage = (float)p/(float)scaleModifierCount;

                        //System.out.println("positionModifierPercentage "+scaleModifierPercentage);

                        if(scaleModifierPercentage > animationGroupPercentage){
                            if(closestNextScale == null){
                                closestNextScale = animationGroup.getScaleModifiers().get(p);
                                lastNextScaleModifierPercentage = scaleModifierPercentage;
                            }
                            else if(scaleModifierPercentage-animationGroupPercentage < lastNextScaleModifierPercentage-animationGroupPercentage){
                                closestNextScale = animationGroup.getScaleModifiers().get(p);
                                lastNextScaleModifierPercentage = scaleModifierPercentage;
                            }
                        }else{
                            if(closestPreviousScale == null) {
                                closestPreviousScale = animationGroup.getScaleModifiers().get(p);
                                lastPreviousScaleModifierPercentage = scaleModifierPercentage;
                            }
                            else if(animationGroupPercentage-scaleModifierPercentage < animationGroupPercentage-lastPreviousScaleModifierPercentage){
                                closestPreviousScale = animationGroup.getScaleModifiers().get(p);
                                lastPreviousScaleModifierPercentage = scaleModifierPercentage;
                            }
                        }
                    }

                    float percentageBetweenScaleModifiers =
                            (animationGroupPercentage-lastPreviousScaleModifierPercentage)/
                                    (lastNextScaleModifierPercentage-lastPreviousScaleModifierPercentage);

                    float newXScaleMod = 0;
                    float newYScaleMod = 0;
                    float newZScaleMod = 0;

                    float previousScaleX = 0;
                    float previousScaleY = 0;
                    float previousScaleZ = 0;

                    float nextScaleX = 0;
                    float nextScaleY = 0;
                    float nextScaleZ = 0;

                    if(closestPreviousScale != null) {

                        previousScaleX = closestPreviousScale.x;
                        previousScaleY = closestPreviousScale.y;
                        previousScaleZ = closestPreviousScale.z;

                    }else if(closestNextScale != null) {

                        previousScaleX = closestNextScale.x;
                        previousScaleY = closestNextScale.y;
                        previousScaleZ = closestNextScale.z;

                    }

                    if(closestNextScale != null) {

                        nextScaleX = closestNextScale.x;
                        nextScaleY = closestNextScale.y;
                        nextScaleZ = closestNextScale.z;

                    }else if(closestPreviousScale != null) {

                        nextScaleX = closestPreviousScale.x;
                        nextScaleY = closestPreviousScale.y;
                        nextScaleZ = closestPreviousScale.z;

                    }

                    newXScaleMod = previousScaleX+((nextScaleX-previousScaleX)*percentageBetweenScaleModifiers);
                    newYScaleMod = previousScaleY+((nextScaleY-previousScaleY)*percentageBetweenScaleModifiers);
                    newZScaleMod = previousScaleZ+((nextScaleZ-previousScaleZ)*percentageBetweenScaleModifiers);

                    for(int s=0; s<animationGroup.getShapes().size(); s++){

                        gd_gfx_Shape shape = animationGroup.getShapes().get(s);

                        shape.getScale().x = shape.getOriginalScale().x+newXScaleMod;
                        shape.getScale().y = shape.getOriginalScale().y+newYScaleMod;
                        shape.getScale().z = shape.getOriginalScale().z+newZScaleMod;

                    }



                    if (animationGroup.getModelRotationModifiers().size() > 0) {

                        Vector3f closestPreviousMRotation = null;
                        //float lastPreviousMRotationModifierPercentage = 2;
                        Vector3f closestNextMRotation = null;
                        //float lastNextMRotationModifierPercentage = 1;

                        int mRotationModifierCount = animationGroup.getModelRotationModifiers().size();

                        /*for (int p = 0; p < mRotationModifierCount; p++) {
                            float mRotationModifierPercentage = (float) p / (float) mRotationModifierCount - 1;

                        for 3 that would be:

                            0 = 0/3 = 0
                            1 = 1/3 = 0.33
                            2 = 2/3 = 0.66

                           but should be:

                            0 = 0/2 = 0
                            1 = 1/2 = 0.5
                            2 = 2/2 = 1

                            0 = 0/1 = 0
                            1 = 1/1 = 1

                        }*/

                        float mRotationPoint = (animationGroupPercentage * (animationGroup.getModelRotationModifiers().size() - 1));
                        int mRotationPointRound = Math.round(mRotationPoint);

                        Vector3f closestMRotation = animationGroup.getModelRotationModifiers().get(mRotationPointRound);

                        float percentageBetweenMRotationModifiers;

                        if (mRotationPoint == 0) {
                            closestNextMRotation = animationGroup.getModelRotationModifiers().get(1);
                            closestPreviousMRotation = animationGroup.getModelRotationModifiers().get(0);
                            percentageBetweenMRotationModifiers = mRotationPoint;
                        } else if(mRotationPoint == mRotationModifierCount-1) {
                            closestNextMRotation = animationGroup.getModelRotationModifiers().get(mRotationModifierCount-1);
                            closestPreviousMRotation = animationGroup.getModelRotationModifiers().get(mRotationModifierCount-2);
                            percentageBetweenMRotationModifiers = mRotationPoint - (mRotationModifierCount-1);
                        } else if (mRotationPointRound > mRotationPoint) {
                            closestNextMRotation = animationGroup.getModelRotationModifiers().get(mRotationPointRound);
                            closestPreviousMRotation = animationGroup.getModelRotationModifiers().get(mRotationPointRound - 1);
                            percentageBetweenMRotationModifiers = mRotationPoint - (mRotationPointRound-1);
                        } else {
                            closestPreviousMRotation = animationGroup.getModelRotationModifiers().get(mRotationPointRound);
                            closestNextMRotation = animationGroup.getModelRotationModifiers().get(mRotationPointRound + 1);
                            percentageBetweenMRotationModifiers = mRotationPoint - (mRotationPointRound);
                        }

                        float newXMRotationMod = 0;
                        float newYMRotationMod = 0;
                        float newZMRotationMod = 0;

                        float previousMRotationX = 0;
                        float previousMRotationY = 0;
                        float previousMRotationZ = 0;

                        float nextMRotationX = 0;
                        float nextMRotationY = 0;
                        float nextMRotationZ = 0;

                        if (closestPreviousMRotation != null) {

                            previousMRotationX = closestPreviousMRotation.x;
                            previousMRotationY = closestPreviousMRotation.y;
                            previousMRotationZ = closestPreviousMRotation.z;

                        } else if (closestNextMRotation != null) {

                            previousMRotationX = closestNextMRotation.x;
                            previousMRotationY = closestNextMRotation.y;
                            previousMRotationZ = closestNextMRotation.z;

                        }

                        if (closestNextMRotation != null) {

                            nextMRotationX = closestNextMRotation.x;
                            nextMRotationY = closestNextMRotation.y;
                            nextMRotationZ = closestNextMRotation.z;

                        } else if (closestPreviousMRotation != null) {

                            nextMRotationX = closestPreviousMRotation.x;
                            nextMRotationY = closestPreviousMRotation.y;
                            nextMRotationZ = closestPreviousMRotation.z;

                        }

                        newXMRotationMod = previousMRotationX + ((nextMRotationX - previousMRotationX) * percentageBetweenMRotationModifiers);
                        newYMRotationMod = previousMRotationY + ((nextMRotationY - previousMRotationY) * percentageBetweenMRotationModifiers);
                        newZMRotationMod = previousMRotationZ + ((nextMRotationZ - previousMRotationZ) * percentageBetweenMRotationModifiers);

                        getRotation().setX(getRotation().x + newXMRotationMod);
                        getRotation().setY(getRotation().y + newYMRotationMod);
                        getRotation().setZ(getRotation().z + newZMRotationMod);

                    }

                    for(int s=0; s<animationGroup.getShapes().size(); s++) {

                        gd_gfx_Shape shape = animationGroup.getShapes().get(s);

                        if (animationGroup.getFrames().size() > 0) {
                            //float mRotationPoint = (animationGroupPercentage * (animationGroup.getModelRotationModifiers().size() - 1));

                            int frameIndex = (int)Math.ceil((double)(animationGroupPercentage * (animationGroup.getFrames().size())));
                            frameIndex = frameIndex-1;
                            //if (frameIndex < 0) frameIndex = 0;
                            //if (frameIndex >= animationGroup.getFrames().size()) frameIndex = animationGroup.getFrames().size()-1;
                            gd_gfx_Texture texture = animationGroup.getFrames().get(frameIndex);
                            //System.out.println("frameIndex = "+frameIndex);
                            shape.setTexture(texture, false);

                        }

                    }

                }

            }

        }
    }

    public void resetAnimations() {
        for (int s=0; s<shapes.size(); s++) {
            shapes.get(s).setRotation(shapes.get(s).getOriginalRotation());
            shapes.get(s).setPosition(shapes.get(s).getOriginalPosition());
            shapes.get(s).setScale(shapes.get(s).getOriginalScale());
            shapes.get(s).setTexture(shapes.get(s).getOriginalTexture(), false);
        }
        setRotation(originalRotation);
    }

    public Vector3f getRotation() {
        return rotation;
    }
    public Vector3 getRotation2() {
        return gd_Math.convertVector(getRotation());
    }

    public void setRotation(Vector3f rotation) {
        setRotation(rotation,true);
    }
    public void setRotation(Vector3 rotation) {
        setRotation(new Vector3f(rotation.x, rotation.y, rotation.z),true);
    }
    public void addRotation(Vector3f rotation) {
        setRotation(this.getRotation().translate(rotation.x, rotation.y, rotation.z), true);
    }
    public void setRotation(Vector3f rotation, boolean setOriginal) {
        setRotation(rotation.x, rotation.y, rotation.z, setOriginal);
    }

    public void setRotation(float x, float y, float z, boolean setOriginal) {
        this.rotation.setX(x);
        this.rotation.setY(y);
        this.rotation.setZ(z);
        if (setOriginal) {
            originalRotation.set(rotation);
        }
    }

    /*public void setRepeatTexture (boolean repeatTexture) {
        for (int s=0; s<shapes.size(); s++) {
            shapes.get(s).setRepeatTexture(repeatTexture);
        }
    }

    public void setTextureWidth (float textureWidth) {
        for (int s=0; s<shapes.size(); s++) {
            shapes.get(s).setTextureWidth(textureWidth);
        }
    }

    public void setTextureHeight (float textureHeight) {
        for (int s=0; s<shapes.size(); s++) {
            shapes.get(s).setTextureHeight(textureHeight);
        }
    }*/

    public gd_gfx_Animation getActiveAnimation() {
        return activeAnimation;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public gd_gfx_Model clone() {
        gd_gfx_Model clone = new gd_gfx_Model();
        clone.getAnimations().addAll(animations);

        for (int s=0; s<shapes.size(); s++) {
            clone.getShapes().add(shapes.get(s).clone());
        }
        //clone.getShapes().addAll(shapes);
        clone.getPosition().set(position);
        clone.getRotation().set(rotation);
        clone.hasFlatTexture = hasFlatTexture;
        return clone;
    }

    public BoundingBox calculateRotatedBounds()
    {
        ArrayList<BoundingBox> bounds = new ArrayList<BoundingBox>();
        for(int t = 0; t < getShapes().size(); t++)
        {
            gd_gfx_Shape shape = getShapes().get(t);
            bounds.add(shape.getRotatetBoundingBox());
        }
        Quaternion rot = new Quaternion().setFromAxis(gd_Math.convertVector(rotation), 0);
        BoundingBox retBounds = new BoundingBox();
        for(int i = 0; i < bounds.size(); i++)
        {
            Vector3 min = bounds.get(i).min.add(gd_Math.convertVector(position)).mul(rot);
            Vector3 max = bounds.get(i).max.add(gd_Math.convertVector(position)).mul(rot);
            retBounds.ext(min);
            retBounds.ext(max);
        }

        return retBounds;
    }

    public float getAnimationPoint() {
        return animationPoint;
    }

    public void setAnimationPoint(float animationPoint) {
        this.animationPoint = animationPoint;
    }

    public void setActiveAnimation(gd_gfx_Animation activeAnimation) {
        this.activeAnimation = activeAnimation;
    }
}

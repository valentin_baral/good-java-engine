package tests;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import good.engine.entity.graphics.gd_gfx_Shape;
import good.engine.gd_BasicResources;
import good.engine.main.CoreComponent;
import good.engine.main.DaggerCoreComponent;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.lwjgl.util.vector.Vector3f;

import static org.junit.jupiter.api.Assertions.*;

class gd_gfx_ShapeTest {

    private gd_BasicResources resources;

    @BeforeEach
    void setUp() {
        CoreComponent coreInjector = DaggerCoreComponent.create();
        resources = coreInjector.getResources();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void verySimpleBoudningBox() {
        gd_gfx_Shape shape = resources.buildBoxShape(new Vector3f(100,100,100));
        BoundingBox box = shape.getRotatetBoundingBox();
        assertEquals(50, box.max.x,0.1, "Max x not working");
        assertEquals(50, box.max.z,0.1, "Max z not working");
        assertEquals(-50, box.min.y,0.1, "Min y not working");
        //shape.setRotation(new Vector3(90,0,0));
    }

    @Test
    void verySimpleRotatedBoudningBox() {
        gd_gfx_Shape shape = resources.buildBoxShape(new Vector3f(100,100,100));
        shape.setRotation(new Vector3(45,0,0));
        BoundingBox box = shape.getRotatetBoundingBox();
        assertEquals(70.7, box.max.y,0.1, "Max y not working");
        assertEquals(70.7, box.max.z,0.1, "Max z not working");
        assertEquals(-50, box.min.x,0.1, "Min x not working");
        //shape.setRotation(new Vector3(90,0,0)); 141.4
    }
}
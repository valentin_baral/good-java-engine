package good.engine.moduleManager.modules.soundGenerator;

import java.util.ArrayList;

/**
 * Created by Valentin on 05/08/2015.
 */
public class sg_component {

    //public static enum types {ANY, LINE, BUILD, PAY, DROP} //make these terms user generated

    //private int currentNote = 0;
    private ArrayList<sg_note> notes = new ArrayList<sg_note>();
    private ArrayList<sg_component> components = new ArrayList<sg_component>();

    private sg_channelType type;

    public sg_component(sg_channelType type){
        this.type = type;
    }

    public void addNote(sg_note Note){
        notes.add(Note);
    }

    public void addNoteBreak(int size){
        sg_note note = new sg_note(sg_note.types.EIGHTHS,0,0,0,0,null);
        note.setBlank();

        for(int i=0; i<size; i++){
            notes.add(note);
        }
    }

    public void addNotes(ArrayList<sg_note> sg_notes){
        this.notes.addAll(sg_notes);
    }

    public sg_channelType getType() {
        return type;
    }

    public ArrayList<sg_note> getNotes() {
        return notes;
    }

    public long calcLength(sg_channel chan){

        long totaltime = 0;

        float speed;

        for(int i=0; i<notes.size(); i++) {

            sg_note note = notes.get(i);

            int multi = 0;

            switch(note.getType()){
                case SIXTEENS:
                    multi = 1;
                    break;
                case EIGHTHS:
                    multi = 2;
                    break;
                case QUARTER:
                    multi = 4;
                    break;
                case HALF:
                    multi = 8;
                    break;
                case FULL:
                    multi = 16;
                    break;
            }

            speed = 1*chan.getSpeed();

            int size = (int)(sg_s_synthesizer.SAMPLE_RATE*sg_s_synthesizer.CHANNELS*speed)*multi;

            totaltime += size;


        }

        return totaltime;


    }

    public ArrayList<sg_component> getComponents() {
        return components;
    }

    public ArrayList<sg_component> getAllComponents() {

        ArrayList<sg_component> modcomponents = new ArrayList<sg_component>();

        modcomponents.addAll(components);
        modcomponents.add(this);

        return modcomponents;
        
    }

    public void addComponent(sg_component component){
        components.add(component);
    }
}

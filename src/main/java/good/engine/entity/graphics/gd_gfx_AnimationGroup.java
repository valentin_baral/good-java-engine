package good.engine.entity.graphics;

import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;

/**
 * Created by valentinbaral on 30/12/2015.
 */
public class gd_gfx_AnimationGroup {

    private float startPoint = 0f; // 0-1
    private float endPoint = 1f; // 0-1

    private ArrayList<gd_gfx_Shape> shapes = new ArrayList<gd_gfx_Shape>();
    private ArrayList<Vector3f> positionModifiers = new ArrayList<Vector3f>();
    private ArrayList<Vector3f> scaleModifiers = new ArrayList<Vector3f>();
    private ArrayList<Vector3f> modelRotationModifiers = new ArrayList<Vector3f>();
    private ArrayList<gd_gfx_Texture> frames = new ArrayList<gd_gfx_Texture>();

    public gd_gfx_AnimationGroup(){

    }

    public ArrayList<gd_gfx_Shape> getShapes() {
        return shapes;
    }

    public float getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(float startPoint) {
        this.startPoint = startPoint;
    }

    public float getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(float endPoint) {
        this.endPoint = endPoint;
    }

    public ArrayList<Vector3f> getPositionModifiers() {
        return positionModifiers;
    }

    public ArrayList<Vector3f> getScaleModifiers() {
        return scaleModifiers;
    }

    public ArrayList<gd_gfx_Texture> getFrames() {
        return frames;
    }

    public ArrayList<Vector3f> getModelRotationModifiers() {
        return modelRotationModifiers;
    }

}

package good.math;


import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.Random;

public class gd_Math {

    public static Vector3f convertVector(com.badlogic.gdx.math.Vector3 input)
    {
        return new Vector3f(input.x, input.y, input.z);
    }
    public static com.badlogic.gdx.math.Vector3 convertVector(Vector3f input)
    {
        return new com.badlogic.gdx.math.Vector3(input.x, input.y, input.z);
    }


    public static float clamp(float val, float min, float max) {
        return Math.max(min, Math.min(max, val));
    }

    /**
     * Implementing Fisher–Yates shuffle
     * copy from https://stackoverflow.com/questions/1519736/random-shuffling-of-an-array
     * @param ar
     */
    public static void shuffleFloatArray(float[] ar)
    {
        // If running on Java 6 or older, use `new Random()` on RHS here
        Random rnd = new Random();
        for (int i = ar.length - 1; i > 0; i--)
        {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            float a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }
    public static int vectorComperator(Vector2f a, Vector2f b)
    {
        int x = Double.compare(a.x, b.x);
        if(x == 0) x = Double.compare(a.y, b.y);
        return x;
    }
    public static int vectorComperator(com.badlogic.gdx.math.Vector3 a, com.badlogic.gdx.math.Vector3 b)
    {
        int x = Float.compare(a.x, b.x);
        if(x == 0) x = Float.compare(a.y, b.y);
        if(x == 0) x = Float.compare(a.y, b.y);
        return x;
    }
    public static com.badlogic.gdx.math.Vector3 getMin(ArrayList<com.badlogic.gdx.math.Vector3> bounds) {
        return bounds.stream().min((x, y) -> gd_Math.vectorComperator(x,y)).get();
    }

    public static com.badlogic.gdx.math.Vector3 getMax(ArrayList<com.badlogic.gdx.math.Vector3> bounds) {
        return bounds.stream().max((x, y) -> gd_Math.vectorComperator(x,y)).get();
    }


    public static boolean randomBoolean()
    {
        return Math.random() < 0.5;
    }
    public static float randomRange(float start, float stop)
    {
        return new Random().nextFloat()*(stop-start)+start;
    }

    public static Vector3f addOnto(Vector3f a, Vector3f b)
    {
        a.x += b.x;
        a.y += b.y;
        a.z += b.z;
        return a;
    }
    public static Vector3f multiplyOnto(Vector3f a, Vector3f b)
    {
        a.x *= b.x;
        a.y *= b.y;
        a.z *= b.z;
        return a;
    }
}

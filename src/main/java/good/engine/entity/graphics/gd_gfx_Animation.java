package good.engine.entity.graphics;

import java.util.ArrayList;

/**
 * Created by valentinbaral on 30/12/2015.
 */
public class gd_gfx_Animation {

    private ArrayList<gd_gfx_AnimationGroup> animationGroups = new ArrayList<gd_gfx_AnimationGroup>();

    private float timeLength = 1f; // 1 == 1sec

    private boolean loop = false;

    private boolean stayOnLast = false;

    private String name;

    public gd_gfx_Animation(String name){
        this.name = name;
    }

    public ArrayList<gd_gfx_AnimationGroup> getAnimationGroups() {
        return animationGroups;
    }

    public void addAnimationGroup(gd_gfx_AnimationGroup animationGroup){
        animationGroups.add(animationGroup);
    }

    public float getTimeLength() {
        return timeLength;
    }

    public void setTimeLength(float timeLength) {
        this.timeLength = timeLength;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLoop() {
        return loop;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public boolean isStayOnLast() {
        return stayOnLast;
    }

    public void setStayOnLast(boolean stayOnLast) {
        this.stayOnLast = stayOnLast;
    }
}

package good.engine.moduleManager.modules.soundGenerator;

import good.engine.main.gd_Config;
import good.engine.output.audio.gd_output_AudioLine;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by valentinbaral on 12/08/15.
 */
public class sg_channel {

    // some attrs

    private Random rand = new Random();

    private long genTime;

    private float speed;

    private sg_lottery Lottery;

    private sg_channelType lastComponentType = null;

    private sg_channelType startType = null;

    private ArrayList<sg_component> components = new ArrayList<sg_component>();
    private ArrayList<sg_rule> rules = new ArrayList<sg_rule>();

    //private ArrayList<sg_component_data> queue = new ArrayList<sg_component_data>();

    //private sg_channel_player player;



    public sg_channel(float speed){

        this.speed = speed;

        genTime = gd_output_AudioLine.position.get();

        Lottery = new sg_lottery();

    }

    public void update(){

        // super shit
        if(gd_output_AudioLine.position.get()+(gd_output_AudioLine.SAMPLE_RATE*gd_Config.updatesPerSecond)>genTime){
            for(int i=0; i<3; i++){
                System.out.println("gen comp");
                genComponent();
            }
        }

    }

    private void genComponent(){

        if(components.size() < 1) return;

        for(int i = 0; i < rules.size(); i++){
            checkRule(lastComponentType, rules.get(i));
        }

        sg_channelType ticket = Lottery.pull();

        if(ticket == null) return;

        sg_component component = getComponentOfType(ticket);

        if(component == null) return;

        System.out.println("dogen");

        long newtime = sg_s_synthesizer.gen(this, component, genTime);
        
        genTime += newtime;

        lastComponentType = component.getType();

    }

    private sg_component getComponentOfType(sg_channelType type){

        ArrayList<sg_component> componentsOfType = new ArrayList<sg_component>();

        for(int i = 0; i<components.size(); i++){
            if(components.get(i).getType() == type){
                componentsOfType.add(components.get(i));
            }
        }

        if(componentsOfType.size()<1) return null;

        return componentsOfType.get(rand.nextInt(componentsOfType.size()));

    }

    /*private sg_component.types getLastComponent(){
        if(queue.size()>0){
            return queue.get(queue.size()-1).getType();
        }

        return null;
    }*/

    private void checkRule(sg_channelType component, sg_rule rule){

        if(component == null) {
            Lottery.addTickets(startType,1);
            return;
        }

        switch (rule.getType()){
            case NOT:

                if(component == rule.getComp1()){
                    Lottery.exclude(rule.getComp2());
                }

                break;
            case NOT_UNTIL:

                if(false == rule.isTriggered()){

                    if(component == rule.getComp1()){
                        rule.setTriggered(true);
                    }

                }

                /*if(component.getType() == rule.getComp1()){
                    Lottery.excludeUntil(rule.getComp2());
                }*/

                break;
            case CHANCE:

                if(component == rule.getComp1()){
                    Lottery.addTickets(rule.getComp2(),(int)rule.getChance()*10);
                }

                break;
        }

    }

    /*public int getCurrentFreq(){
        return player.getCurrentFreq();
    }*/

    /*private long countQueueTime(){
        long count = 0;

        for(int i=0; i<queue.size()-1; i++){
            if(queue.get(i).getAudioData()!=null)count+=queue.get(i).getAudioData().length;
        }

        return count;
    }*/


    public void setGenTime(long genTime){
        this.genTime = genTime;
    }

    public void addComponent(sg_component component){
        components.add(component);
    }
    public ArrayList<sg_component> getComponents(){
        return components;
    }

    public void addRule(sg_rule rule){
        rules.add(rule);
    }
    public ArrayList<sg_rule> getRules(){
        return rules;
    }

    public float getSpeed() {
        return speed;
    }

    public void startWith(sg_channelType type){
        startType = type;
    }

}

package good.engine.moduleManager;

import good.engine.input.gd_Input;
import good.engine.main.gd_Build;
import good.engine.output.render.gd_output_Renderer;

/**
 * Created by valentinbaral on 02/11/2015.
 */
public abstract class gd_Module {

    private int weight = 0;

    private String name = "";

    public gd_Module(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight(){
        return weight;
    }

    public void setWeight(int weight){
        this.weight = weight;
    }

    public void init(gd_Build engine){}

    public void takeInput(gd_Input input){}
    public void takePolledInput(gd_Input input){}

    public void preUpdate(gd_Build engine){}
    public void update(gd_Build engine){}
    public void postUpdate(gd_Build engine){}

    public void preDraw(gd_output_Renderer renderer){}
    public void draw(gd_output_Renderer renderer){}
    public void postDraw(gd_output_Renderer renderer){}

    public void drawHud(gd_output_Renderer renderer){}

}

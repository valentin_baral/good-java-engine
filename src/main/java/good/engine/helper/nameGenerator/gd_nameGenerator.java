package good.engine.helper.nameGenerator;

import good.engine.helper.gd_Math;

import java.util.ArrayList;

/**
 * Created by valentinbaral on 22/12/2016.
 */
public class gd_nameGenerator {

    private static boolean defaultLettersGenerated = false;

    private static ArrayList<gd_nameGenerator_letter> letters = new ArrayList<gd_nameGenerator_letter>();

    public static String generateName(){
        return generateName((gd_Math.random.nextInt(3)+1)*(gd_Math.random.nextInt(6)+1));
    }

    public static String generateName(int length){

        System.out.println("generate name with length: "+String.valueOf(length));

        if(defaultLettersGenerated == false) generateDefaultLetterSet();

        return generateName(length, letters);
    }

    public static String generateName(int length, ArrayList<gd_nameGenerator_letter> letters){

        String name = "";
        gd_nameGenerator_letter previousLetter = null;

        for(int l=0; l<length; l++){

            gd_nameGenerator_letter nextLetter = getNextLetter(letters, previousLetter, (l==length-1));
            name += nextLetter.getLetter();

            System.out.println("Get next letter: "+nextLetter.getLetter());

        }

        return name;

    }

    public static gd_nameGenerator_letter getNextLetter(ArrayList<gd_nameGenerator_letter> letters, gd_nameGenerator_letter previousLetter, boolean isLast){

        ArrayList<Integer> tickets = new ArrayList<Integer>();

        for(int l=0; l<letters.size(); l++){

            gd_nameGenerator_letter letter = letters.get(l);

            float probability = letter.getBaseProbability();

            if(previousLetter == null){
                probability *= letter.getFirstLetterProbability();
            }
            if(isLast){
                probability *= letter.getLastLetterProbability();
            }
            if(previousLetter != null) {
                if(previousLetter == letter) probability /= 2;
                if (previousLetter.getConnectingLetterProbability() <= 1f) {
                    probability *= letter.getConnectingLetterProbability();
                } else {
                    probability /= letter.getConnectingLetterProbability();
                }
            }

            System.out.println(probability);

            Double ticketCountDouble = gd_Math.mRound((probability*100), 1);

            int ticketCount = ticketCountDouble.intValue();

            for(int t=0; t<ticketCount; t++){
                tickets.add(l);
            }

        }

        int ticket = tickets.get(gd_Math.random.nextInt(tickets.size()-1));

        gd_nameGenerator_letter selectedLetter = letters.get(ticket);

        return selectedLetter;

    }

    public static void generateDefaultLetterSet(){

        String[] regularCharacters = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
        float[] baseProbabilityModifiers = {0.081f, 0.015f, 0.028f, 0.043f, 0.127f, 0.022f, 0.02f, 0.061f, 0.07f, 0.002f, 0.01f, 0.04f, 0.024f, 0.067f, 0.075f, 0.019f, 0.001f, 0.06f, 0.063f, 0.09f, 0.027f, 0.01f, 0.024f, 0.002f, 0.019f, 0.001f};
        float[] connectionLetterProbabilityModifiers = {3f, 1f, 1f, 1f, 4f, 1f, 1f, 1f, 4f, 1f, 1f, 1f, 1f, 1f, 2f, 1f, 1f, 1f, 1f, 1f, 2f, 1f, 1f, 1f, 1f, 1f};

        for(int c=0; c<regularCharacters.length; c++){

            gd_nameGenerator_letter letter = new gd_nameGenerator_letter(regularCharacters[c], baseProbabilityModifiers[c]*10);

            letter.setConnectingLetterProbability(connectionLetterProbabilityModifiers[c]);

            letters.add(letter);

        }

        defaultLettersGenerated = true;

    }

}

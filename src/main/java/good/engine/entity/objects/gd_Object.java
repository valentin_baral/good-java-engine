package good.engine.entity.objects;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import good.engine.entity.graphics.gd_gfx_Model;
import good.engine.entity.graphics.gd_gfx_Shape;
import good.engine.entity.graphics.gd_gfx_Triangle;
import good.engine.entity.hitbox.gd_Hitbox;
import good.engine.input.gd_Input;
import good.engine.main.gd_Build;
import good.engine.main.gd_Config;
import good.engine.output.render.gd_output_Renderer;
import good.math.gd_Math;
import org.lwjgl.util.vector.Vector3f;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by valentinbaral on 17/11/2015.
 */
public class gd_Object {

    // optionally name object for access
    protected String name = "object";

    // this determines whether object will be updated & drawn by the good.engine
    protected boolean active = true;

    // this determines whether object will be updated & drawn by the good.engine 2
    protected boolean active2 = true;

    // if false, object can be collided with but won't check against other objects
    protected boolean physical = false;

    // if false, object cant be collided with
    protected boolean solid = true;

    // if true, this is a trigger and won't make physical collision but can have another effect
    protected boolean trigger = false;

    // if false, object doesnt have gravity
    protected boolean gravity = true;

    protected boolean collisionTop = false;
    protected boolean collisionBottom = false;
    protected boolean collisionLeft = false;
    protected boolean collisionRight = false;
    protected boolean collisionFront = false;
    protected boolean collisionBack = false;

    private float gravityTime = 0;

    //protected Vector3f originalPosition = new Vector3f(0,0,0);
    protected Vector3f position = new Vector3f(0,0,0);
    protected Vector3f nextPosition = new Vector3f(0,0,0);

    protected Vector3f direction = new Vector3f(0,0,0);
    protected float velocityMultiplier = 50;
    protected boolean velocity = false;

    protected ArrayList<gd_Hitbox> hitboxes = new ArrayList<gd_Hitbox>();

    protected gd_gfx_Model model = null;

    public gd_Object(){

        init();

    }

    public void init(){

        /*
        leave empty
         */

    }

    public void preUpdate(gd_Build engine){

    }

    public void update(gd_Build engine){

    }

    public void postUpdate(gd_Build engine){

    }

    public void preDraw(gd_output_Renderer renderer){

    }

    public void draw(gd_output_Renderer renderer){

    }

    public void postDraw(gd_output_Renderer renderer){

    }

    public void takeInput(gd_Input input){

    }

    public void takePolledInput(gd_Input input){

    }

    public void onCollision(gd_Object object){
        // don't put movement calls here
    }

    public void velocity(){

        if(!velocity) return;

        moveOnX(velocityMultiplier*(float)Math.sin(Math.toRadians(direction.y)) * gd_Config.hardDeltaTime());
        moveOnY(velocityMultiplier*(float)-Math.tan(Math.toRadians(direction.x)) * gd_Config.hardDeltaTime());
        moveOnZ(velocityMultiplier*(float)-Math.cos(Math.toRadians(direction.y)) * gd_Config.hardDeltaTime());

    }

    public void gravity(){

        if(!isPhysical() || !gravity) return;
        gravityTime += gd_Config.hardDeltaTime();
        if(collisionBottom) gravityTime = 0;


        moveOnY(-(((gravityTime/1f) * gd_Config.gravity + gd_Config.gravity) * gd_Config.hardDeltaTime()));
        //moveOnY(-(gd_Config.gravity+((float)gravityCount)) * 120f * gd_Config.hardDeltaTime());

    }

    public Vector3f  getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {

        setPosition(position.x, position.y, position.z);

    }
    public void setPosition(Vector3 position) {

        setPosition(position.x, position.y, position.z);

    }

    public void setPosition(float x, float y, float z) {


        // important for hit detection
        nextPosition.set(x,y,z);

        position.set(x,y,z);

    }

    public Vector3f getNextPosition() {
        return nextPosition;
    }

    public void setNextPosition(Vector3f nextPosition) {
        this.nextPosition = nextPosition;
    }

    public ArrayList<gd_Hitbox> getHitboxes() {
        return hitboxes;
    }

    public void addHitbox(gd_Hitbox hitbox){
        hitboxes.add(hitbox);
    }

    public void addHitboxes(ArrayList<gd_Hitbox> hitboxes){
        this.hitboxes.addAll(hitboxes);
    }

    public gd_gfx_Model getModel() {
        return model;
    }

    public void setModel(gd_gfx_Model model) {
        this.model = model;
    }

    public void move(){
        position.set(nextPosition);
    }

    public void moveOnX(float mod){
        nextPosition.x += mod;
    }

    public void moveOnY(float mod){
        nextPosition.y += mod;
    }

    public void moveOnZ(float mod){
        nextPosition.z += mod;
    }

    public void moveUp(float mod) {
        if(!isCollisionTop()) moveOnY(mod);
    }

    public void moveDown(float mod) {
        if(!isCollisionBottom()) moveOnY(mod);
    }

    public void moveLeft(float mod) {
        if(!isCollisionLeft()) moveOnX(mod);
    }

    public void moveRight(float mod) {
        if(!isCollisionRight()) moveOnX(mod);
    }

    public void moveForward(float mod) {
        if(!isCollisionFront()) moveOnZ(mod);
    }

    public void moveBackwards(float mod) {
        if(!isCollisionBack()) moveOnZ(mod);
    }

    public void setCollision(boolean collisionTop, boolean collisionBottom, boolean collisionLeft, boolean collisionRight, boolean collisionFront, boolean collisionBack){
        this.collisionTop = collisionTop;
        this.collisionBottom = collisionBottom;
        this.collisionLeft = collisionLeft;
        this.collisionRight = collisionRight;
        this.collisionFront = collisionFront;
        this.collisionBack = collisionBack;
    }

    public boolean isCollisionTop() {
        return collisionTop;
    }

    public boolean isCollisionBottom() {
        return collisionBottom;
    }

    public boolean isCollisionLeft() {
        return collisionLeft;
    }

    public boolean isCollisionRight() {
        return collisionRight;
    }

    public boolean isCollisionFront() {
        return collisionFront;
    }

    public boolean isCollisionBack() {
        return collisionBack;
    }

    public boolean isPhysical() {
        return physical;
    }

    public void setPhysical(boolean physical) {
        this.physical = physical;
    }

    public boolean isSolid() {
        return solid;
    }

    public void setSolid(boolean solid) {
        this.solid = solid;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive2() {
        return active2;
    }

    public void setActive2(boolean active2) {
        this.active2 = active2;
    }

    public float getVelocityMultiplier() {
        return velocityMultiplier;
    }

    public void setVelocityMultiplier(float velocityMultiplier) {
        this.velocityMultiplier = velocityMultiplier;
    }

    public Vector3f getDirection() {
        return direction;
    }

    public void setDirection(float x, float y, float z) {
        direction.set(x,y,z);
    }

    public void setDirection(Vector3f direction) {
        setDirection(direction.x,direction.y,direction.z);
    }

    public boolean isGravity() {
        return gravity;
    }

    public void setGravity(boolean gravity) {
        this.gravity = gravity;
    }

    public boolean isVelocity() {
        return velocity;
    }

    public void setVelocity(boolean velocity) {
        this.velocity = velocity;
    }

    public boolean isTrigger() {
        return trigger;
    }

    public void setTrigger(boolean trigger) {
        this.trigger = trigger;
    }

    public Vector3f calculateBounds(){
        // heavy call as we create new vector shouldn't be in main thread
        // not accurate if hitbox has position other than object center

        Vector3f size = new Vector3f(0,0,0);

        for(int h=0; h<hitboxes.size(); h++){
            gd_Hitbox modHitbox = hitboxes.get(h);

            if(modHitbox.getSize().x > size.x) size.setX(modHitbox.getSize().x);
            if(modHitbox.getSize().y > size.y) size.setY(modHitbox.getSize().y);
            if(modHitbox.getSize().z > size.z) size.setZ(modHitbox.getSize().z);

        }

        return size;
    }


    public BoundingBox calculateRotatedBounds()
    {
        return getModel().calculateRotatedBounds();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vector3 getPosition2() {
        return gd_Math.convertVector(getPosition());
    }
}

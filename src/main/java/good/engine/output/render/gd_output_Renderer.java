package good.engine.output.render;

import java.awt.*;
import java.nio.FloatBuffer;

import com.badlogic.gdx.math.Vector3;
import good.engine.entity.graphics.*;
import good.engine.helper.gd_Math;
import good.engine.main.gd_Build;
import good.engine.main.gd_Config;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import org.lwjgl.util.glu.GLU;

import org.lwjgl.util.vector.Vector3f;

import org.newdawn.slick.TrueTypeFont;

import org.newdawn.slick.Color;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glColorMaterial;
import static org.lwjgl.opengl.GL11.glLightModel;

/**
 * Created by valentinbaral on 03/11/2015.
 */
public class gd_output_Renderer {

    public TrueTypeFont heading;
    public TrueTypeFont normal;
    public TrueTypeFont serif;
    private boolean antiAlias = true;

    // The window handle
    //public long window;

    private gd_output_Camera camera;
    private gd_output_Scene scene;

    public gd_output_Renderer() throws LWJGLException{

        createDisplay();

        camera = new gd_output_Camera();
        scene = new gd_output_Scene();

        normal = new TrueTypeFont(new Font("Courier New", Font.BOLD, 18), antiAlias);
        serif = new TrueTypeFont(new Font("Georgia", Font.BOLD, 42), antiAlias);
        heading = new TrueTypeFont(new Font("Arial", Font.BOLD, 42), antiAlias);

    }

    public void update(){

        if(Display.isCloseRequested()){
            closeWindow();
        }

    }

    private void createDisplay() throws LWJGLException {

        //Display.setDisplayMode(new DisplayMode(gd_Config.WIDTH, gd_Config.HEIGHT));
        //Display.setDisplayModeAndFullscreen(new DisplayMode(gd_Config.WIDTH, gd_Config.HEIGHT));
        Display.setVSyncEnabled(gd_Config.vSyncEnabled); // will limit fps to display refresh rate make dynamic between render times, also feels unsomooth if framerate not consistent at the moment!!!
        Display.setFullscreen(false);
        Display.setResizable(true);
        Display.setTitle(gd_Config.name);
        Display.create();

    }

    public void setUpGraphics(gd_Build engine){

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        // 0.0001f breaks rendering on mac set to 1 to fix
        GLU.gluPerspective(camera.getFieldOfView(), (float)Display.getWidth() / (float)Display.getHeight(), 10f, 2500.f);

        glMatrixMode(GL_MODELVIEW);

        glEnable(GL_DEPTH_TEST);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glDepthMask(true);
        glDepthFunc(GL_LEQUAL);

        glAlphaFunc(GL_GREATER, 0.01f);
        glEnable(GL_ALPHA_TEST);

        enableFog();
        //glDisable(GL_BLEND);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glShadeModel(GL_FLAT);
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glEnable(GL_LIGHT1);
        glEnable(GL_LIGHT2);

        glLightModel(GL_LIGHT_MODEL_AMBIENT, asFlippedFloatBuffer(new float[]{0.2f, 0.2f, 0.2f, 1f}));

        glMaterial(GL_FRONT, GL_AMBIENT, asFlippedFloatBuffer(new float[]{0.5f, 0.5f, 0.5f, 1f}));
        glMaterial(GL_FRONT, GL_DIFFUSE, asFlippedFloatBuffer(new float[]{1f, 1f, 1f, 1f}));

        glEnable(GL_BLEND);
        glDisable(GL_SMOOTH);
        glDisable(GL_LINE_SMOOTH);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


        glPushMatrix();

        // move and rotate
        rotate(camera.getRotation());
        move(camera.getPosition());

    }

    private void enableFog() {
        glEnable(GL_FOG);
        glFog(GL_FOG_COLOR, asFlippedFloatBuffer(new float[]{scene.getSky().x, scene.getSky().y, scene.getSky().z, 1f}));

        glFogi(GL_FOG_MODE, GL_LINEAR);
        glFogf(GL_FOG_START, scene.getFogStart());
        glFogf(GL_FOG_END, scene.getFogEnd());
    }

    public void endGraphics(gd_Build engine){

        glPopMatrix();

        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
        glDisable(GL_LIGHT1);
        glDisable(GL_LIGHT2);

        glBindTexture(GL_TEXTURE_2D, -1);
        //glDisable(GL_TEXTURE_2D);
    }

    public void setUpHudGraphics(gd_Build engine){

        glDisable(GL_TEXTURE_2D);
        glDisable(GL_COLOR_MATERIAL);

        glMatrixMode(GL_PROJECTION);

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        //glLoadIdentity();


        glTranslatef(0, 0, 0);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();


        glEnable(GL_TEXTURE_2D);
        //glEnable(GL_COLOR_MATERIAL);

        glShadeModel(GL_FLAT);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);

        glDisable(GL_CULL_FACE);

        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClearDepth(1);

        glEnable(GL_BLEND);
        //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        //glBlendFunc(GL11.GL_ONE, GL11.GL_SRC_ALPHA);

        glViewport(0, 0, Display.getWidth(), Display.getHeight());
        //glMatrixMode(GL_MODELVIEW);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, Display.getWidth(), Display.getHeight(), 0, 1, -1);
        glMatrixMode(GL_MODELVIEW);

        // ???????????????????
        glBindTexture(GL_TEXTURE_2D, 1); // 1 is the normal but why do I need to do it this way?

    }

    public void endHudGraphics(gd_Build engine){

        //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        // ???????????????????
        glBindTexture(GL_TEXTURE_2D, -1);
        glDisable(GL_TEXTURE_2D);
    }

    public void startDraw(gd_Build engine){

        glClearColor(scene.getSky().x, scene.getSky().y, scene.getSky().z, 1f);

        glClearDepth(1.0f);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        if(gd_Config.wireframe){
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        }else{
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        glLoadIdentity();

    }

    public void endDraw(gd_Build engine){

        Display.update();

    }

    public void drawModelAlignedToCamera(Vector3f position, gd_gfx_Model model) {

        glPushMatrix();

        glTranslatef(camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);

        //Translate the screen to the camera
        glRotatef(-camera.getRotation().y, 0, 1, 0);
        glRotatef(-camera.getRotation().x, 1, 0, 0);

        drawModel(position, model);

        glPopMatrix();

    }

    public void drawModelAlignedToRotation(Vector3f rotation, Vector3f center, Vector3f position, gd_gfx_Model model) {

        glPushMatrix();

        glTranslatef(center.x, center.y, center.z);

        //Translate the screen to the camera
        glRotatef(-rotation.y, 0, 1, 0);
        glRotatef(-rotation.x, 1, 0, 0);

        drawModel(position, model);

        glPopMatrix();

    }

    public void alignScreenToCameraStart() {

        glPushMatrix();

        glTranslatef(camera.getPosition().x, camera.getRotation().y, camera.getPosition().z);

        //Translate the screen to the camera
        glRotatef(-camera.getRotation().y, 0, 1, 0);
        glRotatef(-camera.getRotation().x, 1, 0, 0);

    }

    public void alignScreenToCameraEnd(){

        glPopMatrix();

    }



    public void drawModel(Vector3f position, gd_gfx_Model model){
        drawModel(position.x, position.y, position.z, model);
    }

    public void drawModel(float x, float y, float z, gd_gfx_Model model){

        Vector3f modelPosition = model.getPosition();
        Vector3f modelRotation = model.getRotation();
        Vector3f modelScale = model.getScale();

        // rotate
        glPushMatrix();

        glTranslatef(x + modelPosition.x, y + modelPosition.y, z + modelPosition.z);

        glRotatef(modelRotation.x, 1, 0, 0);
        glRotatef(modelRotation.y, 0, 1, 0);
        glRotatef(modelRotation.z, 0, 0, 1);

        glTranslatef(-(x + modelPosition.x), -(y + modelPosition.y), -(z + modelPosition.z));

        //glRotatef(-modelRotation.y, 0, 1, 0);
        //glRotatef(-modelRotation.x, 1, 0, 0);

        for(int s=0; s<model.getShapes().size(); s++) {

            gd_gfx_Shape shape = model.getShapes().get(s);

            if (shape.ignoreLighting) {

                glDisable(GL_LIGHTING);
                glDisable(GL_LIGHT0);
                glDisable(GL_LIGHT1);
                glDisable(GL_LIGHT2);

            }

            Vector3f shapePosition = shape.getPosition();
            Vector3f shapeRotation = shape.getRotation();
            Vector3f shapeScale = shape.getScale();

            if(shape.hasTexture()){
                //glDisable(GL_COLOR_MATERIAL);
                glEnable(GL_TEXTURE_2D);
                glEnable(GL_COLOR_MATERIAL);
                //glDisable(GL_CULL_FACE);

                Color.white.bind();
                //glColor4f(1.0f, 1.0f, 1.0f, 0);
                //Color.transparent.bind();
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

                //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
                //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);

                /*glEnable(GL_BLEND);
                glEnable(GL_POINT_SMOOTH);
                glEnable(GL_LINE_SMOOTH);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);*/

                //glBlendFunc(GL_ONE, GL_ONE);
                // this essentially reverses the effect
                //glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA);

                glBindTexture(GL_TEXTURE_2D, shape.getTexture().getTexture());

            }else{

                glEnable(GL_COLOR_MATERIAL);

                glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

                String hex = String.format("#%02x%02x%02x", (int)shape.red, (int)shape.green, (int)shape.blue);

                Color.decode(hex).bind();
            }

            // rotate
            glPushMatrix();

            glTranslatef(x + modelPosition.x + shapePosition.x, y + modelPosition.y + shapePosition.y, z + modelPosition.z + shapePosition.z);

            glRotatef(shapeRotation.x, 1, 0, 0);
            glRotatef(shapeRotation.y, 0, 1, 0);
            glRotatef(shapeRotation.z, 0, 0, 1);

            glTranslatef(-(x + modelPosition.x + shapePosition.x), -(y + modelPosition.y + shapePosition.y), -(z + modelPosition.z + shapePosition.z));

            if(shape.isBillboard()) {

                glPushMatrix();


                Vector3 axis = new Vector3(0,0,0);
                if(shape instanceof gd_gfx_BillboardShape)
                {
                    if(!(((gd_gfx_BillboardShape) shape).pivot == null))
                    {
                        axis = ((gd_gfx_BillboardShape) shape).pivot;
                    }
                }

                Vector3 pos = new Vector3(x + modelPosition.x + shapePosition.x + axis.x,y + modelPosition.y + shapePosition.y + axis.y,z + modelPosition.z +shapePosition.z + axis.z);
                //float faceAngle = gd_Math.calcAngle(x + modelPosition.x + (float) axis.x, z + modelPosition.z + (float)axis.z, camera.getPosition().x, camera.getPosition().z);

                float angle = gd_Math.calcAngle((float)pos.x, (float)pos.z, camera.getPosition().x, camera.getPosition().z);

                if(shape instanceof gd_gfx_BillboardShape)
                {
                    final double normalAngle = Math.toRadians(camera.getRotation().y+90);
                    Vector3 normal = new Vector3((float)Math.acos(normalAngle), (float)Math.asin(normalAngle), 0);
                    normal.nor();
                    ((gd_gfx_BillboardShape) shape).updatedNormal = normal;
                }




                //glTranslatef(x + modelPosition.x + shapePosition.x, y + modelPosition.y + shapePosition.y, z + modelPosition.z + shapePosition.z);
                glTranslatef((float) pos.x,(float)pos.y,(float)pos.z);


                glRotatef(-modelRotation.x, 1, 0, 0);
                glRotatef(-modelRotation.y, 0, 1, 0);
                glRotatef(-modelRotation.z, 0, 0, 1);


                glRotatef(-angle, 0, 1, 0);

                //glTranslatef(-(x + modelPosition.x + shapePosition.x), -(y + modelPosition.y + shapePosition.y), -(z + modelPosition.z + shapePosition.z));/
                glTranslatef(-(float)pos.x,-(float)pos.y,-(float)pos.z);

            }

            glBegin(GL_TRIANGLES);

            for (int t = 0; t < shape.getTriangles().size(); t++) {

                gd_gfx_Triangle triangle = shape.getTriangles().get(t);

                // prevent it from draving sides and backside of billboards
                if(!shape.isBillboard() || Math.abs(triangle.getNormals().z + 1f) < 0.001f)
                {
                    for (int p = 0; p < triangle.getPoints().size(); p++) {

                        if(shape instanceof gd_gfx_BillboardShape)
                        {
                            Vector3 normal = ((gd_gfx_BillboardShape) shape).updatedNormal;
                            glNormal3f((float)normal.x, (float)normal.y, (float)normal.z);
                        }
                        else
                        {
                            glNormal3f(triangle.getNormals().x,triangle.getNormals().y,triangle.getNormals().z);
                        }

                        if(shape.hasTexture() ){

                            float tex1 = 0;
                            float tex2 = 0;

                            float shapeWidth = 0;
                            float shapeHeight = 0;


                            if(Math.abs(triangle.getNormals().x) > Math.abs(triangle.getNormals().y) && Math.abs(triangle.getNormals().x) > Math.abs(triangle.getNormals().z)){
                                // x is the greatest normal
                                // this is a triangle at the sides of a cube, which is correct

                                tex1 = Integer.signum((int) (triangle.getPoints().get(p).z * shapeScale.z * modelScale.z));
                                tex2 = Integer.signum((int) (triangle.getPoints().get(p).y * shapeScale.y * modelScale.y));

                                if (triangle.getNormals().x > 0) {
                                    tex1 = tex1 * -1;
                                    //tex2 = tex2 * -1;
                                }

                                shapeWidth = shape.getScale().z * modelScale.z;
                                shapeHeight = shape.getScale().y * modelScale.y;

                            }else if(Math.abs(triangle.getNormals().y) > Math.abs(triangle.getNormals().z)){
                                //y is the greatest

                                tex1 = Integer.signum((int) (triangle.getPoints().get(p).z * shapeScale.z * modelScale.z));
                                tex2 = Integer.signum((int) (triangle.getPoints().get(p).x * shapeScale.x * modelScale.x));

                            /*if (triangle.getNormals().y < 0) {
                                tex1 = tex1 * -1;
                                tex2 = tex2 * -1;
                            }*/

                                shapeWidth = shape.getScale().z * modelScale.z;
                                shapeHeight = shape.getScale().x * modelScale.x;

                            }else{
                                //z is the greatest

                                tex1 = Integer.signum((int) (triangle.getPoints().get(p).x * shapeScale.x * modelScale.x));
                                tex2 = Integer.signum((int) (triangle.getPoints().get(p).y * shapeScale.y * modelScale.y));

                                if (triangle.getNormals().z < 0) {
                                    tex1 = tex1 * -1;
                                    //tex2 = tex2 * -1;
                                }

                                shapeWidth = shapeScale.x * modelScale.x;
                                shapeHeight = shapeScale.y * modelScale.y;
                            }

                            //dir = 2;

                            // 0 = front
                            // 1 = top/bottom
                            // 2 = sides

                        /*
                        need to calculate 1 to be num per side, guess that can be decimal num, so set width height of texture then it will be repeated based on model size
                         */

                            float numWidth = 1;
                            float numHeight = 1;

                            if (shape.getTexture().isRepeatTexture()) {
                                numWidth = shapeWidth / shape.getTexture().getTextureWidth();
                                numHeight = shapeHeight / shape.getTexture().getTextureHeight();
                            }

                        /*if(tex1 == -1) tex1 = 0;
                        else if(tex1 > 0) tex1 = 1;
                        if(tex2 == -1) tex2 = 1;
                        else if(tex2 > 0) tex2 = 0;*/

                            if (shape.isFlipTextureX()) {
                                tex1 *= -1;
                            }
                            if (shape.isFlipTextureY()) {
                                tex2 *= -1;
                            }

                            if(tex1 == -1) tex1 = 0;
                            else if(tex1 > 0) tex1 = 1;
                            if(tex2 == -1) tex2 = 1;
                            else if(tex2 > 0) tex2 = 0;

                            if (tex1 == 1) tex1 = numWidth;
                            if (tex2 == 1) tex2 = numHeight;

                            //glTexCoord2f(tex1, tex2);
                            //glTexCoord2f((float) Math.random(), (float) Math.random());
                            glTexCoord2f(tex1, tex2);

                        }

                        glVertex3f(
                                x + modelPosition.x + shapePosition.x  + (triangle.getPoints().get(p).x * shapeScale.x * modelScale.x),
                                y + modelPosition.y + shapePosition.y * modelScale.y + (triangle.getPoints().get(p).y * shapeScale.y * modelScale.y),
                                z + modelPosition.z + shapePosition.z * modelScale.z + (triangle.getPoints().get(p).z * shapeScale.z * modelScale.z)
                        );

                    }
                }
            }

            glEnd();

            if(shape.hasTexture()){
                glDisable(GL_TEXTURE_2D);
                glDisable(GL_COLOR_MATERIAL);
                //glDisable(GL_BLEND);

            }else{
                glDisable(GL_COLOR_MATERIAL);
            }

            // end rotate
            glPopMatrix();

            if(shape.isBillboard()) {

                glPopMatrix();

            }
            if (shape.ignoreLighting) {

                glEnable(GL_LIGHTING);
                glEnable(GL_LIGHT0);
                glEnable(GL_LIGHT1);
                glEnable(GL_LIGHT2);

            }

        }

        // end rotate
        glPopMatrix();

    }

    public static void drawRect(float x, float y, float width, float height, float r, float g, float b, float a){

        glDisable(GL_TEXTURE_2D);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        Color c = new Color(r,g,b,a);
        c.bind();

        GL11.glBegin(GL11.GL_QUADS);

        //Front
        GL11.glVertex3f(x-width/2, y-height/2, 0);
        GL11.glVertex3f(x+width/2, y-height/2, 0);
        GL11.glVertex3f(x+width/2, y+height/2, 0);
        GL11.glVertex3f(x-width/2, y+height/2, 0);

        GL11.glEnd();

        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        glEnable(GL_TEXTURE_2D);
        glDisable(GL_COLOR_MATERIAL);


    }

    public static void drawRect(float x, float y, float width, float height, gd_gfx_Texture texture){

        glEnable(GL_TEXTURE_2D);
        glEnable(GL_COLOR_MATERIAL);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        Color.white.bind();
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glBindTexture(GL_TEXTURE_2D, texture.getTexture());

        GL11.glBegin(GL11.GL_QUADS);


        //Front
        glTexCoord2f(0, 0);
        GL11.glVertex3f(x-width/2, y-height/2, 0);
        glTexCoord2f(1, 0);
        GL11.glVertex3f(x+width/2, y-height/2, 0);
        glTexCoord2f(1, 1);
        GL11.glVertex3f(x+width/2, y+height/2, 0);
        glTexCoord2f(0, 1);
        GL11.glVertex3f(x-width/2, y+height/2, 0);

        GL11.glEnd();

        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        glEnable(GL_TEXTURE_2D);

    }

    public void drawFont(float x, float y, String text, Color color){

        drawFont(x,y,text,color,normal);

    }

    public void drawFont(float x, float y, String text, Color color, TrueTypeFont font){

        //GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_SRC_ALPHA);

        //Color.white.bind();
        font.drawString(x, y, text, color);

        //GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

    }

    public int getTextWidth(String text){
        return getTextWidth(text, normal);
    }

    public int getTextWidth(String text, TrueTypeFont font){
        return font.getWidth(text);
    }

    public int getTextHeight(){
        return 27;
    }



    public void rotate(Vector3f rotation){

        //Translate the screen to the camera
        glRotatef(rotation.x, 1, 0, 0);
        glRotatef(rotation.y, 0, 1, 0);
        glRotatef(rotation.z, 0, 0, 1);

    }

    public void move(Vector3f position){

        glTranslatef(-position.x, -position.y, -position.z);

    }

    public void light(Vector3f position){

        glTranslatef(position.x,position.y,position.z);

        glLight(GL_LIGHT0, GL_DIFFUSE, asFlippedFloatBuffer(new float[]{scene.getLightDiffuse().x, scene.getLightDiffuse().y, scene.getLightDiffuse().z, scene.getLightDiffuse().w}));
        glLight(GL_LIGHT0, GL_SPECULAR, asFlippedFloatBuffer(new float[]{scene.getLightSpecular().x, scene.getLightSpecular().y, scene.getLightSpecular().z, scene.getLightSpecular().w}));
        glLight(GL_LIGHT0, GL_AMBIENT, asFlippedFloatBuffer(new float[]{scene.getLightAmbient().x, scene.getLightAmbient().y, scene.getLightAmbient().z, scene.getLightAmbient().w}));

        glTranslatef(-position.x,-position.y,-position.z);

    }

    public static FloatBuffer asFlippedFloatBuffer(float... values) {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(values.length);
        buffer.put(values);
        buffer.flip();
        return buffer;
    }

    public gd_output_Camera getCamera() {
        return camera;
    }

    public gd_output_Scene getScene() {
        return scene;
    }

    public static int getDisplayWidth() {
        return Display.getWidth();
    }

    public static int getDisplayHeight() {
        return Display.getHeight();
    }

    public static void closeWindow() {

        Display.destroy();
        System.exit(0);

    }

}



package good.engine.main;

import good.engine.entity.objects.gd_ObjectManager;
import good.engine.input.gd_Input;
import good.engine.moduleManager.modules.SoundManager.gd_SoundManager;
import good.engine.moduleManager.modules.cameraManager.gd_CameraManager;
import good.engine.moduleManager.modules.console.cs_Console;
import good.engine.moduleManager.gd_ModuleManager;
import good.engine.moduleManager.modules.particleEffects.pf_ParticleEffects;
import good.engine.moduleManager.modules.soundGenerator.sg_soundGenerator;
import good.engine.output.gd_Output;
import good.engine.output.render.gd_output_Renderer;
import good.states.gd_State;
import good.states.gd_StateManager;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by valentinbaral on 03/11/2015.
 */
@Singleton
public class gd_Build {

    private gd_Input input;
    private gd_Output output;
    private gd_ModuleManager moduleManager;
    private gd_StateManager stateManager;
    private gd_CameraManager cameraManager;
    private gd_ObjectManager objectManager;
    private sg_soundGenerator soundGenerator;
    private gd_SoundManager soundManager;
    private pf_ParticleEffects particleEffects;
    private cs_Console console;
    public gd_Core core;

    @Inject
    public gd_Build(gd_ModuleManager moduleManager, gd_StateManager stateManager, gd_CameraManager cameraManager, gd_ObjectManager objectManager, sg_soundGenerator soundGenerator, gd_SoundManager soundManager, pf_ParticleEffects particleEffects, cs_Console console, gd_Output output) {
        this.moduleManager = moduleManager;
        this.stateManager = stateManager;
        this.cameraManager = cameraManager;
        this.objectManager = objectManager;
        this.soundGenerator = soundGenerator;
        this.soundManager = soundManager;
        this.particleEffects = particleEffects;
        this.console = console;
        this.input = new gd_Input();
        this.output = output;
        System.out.println("init gd_build");
    }

    public void init(gd_Core core, gd_State game){

        this.core = core;

        System.out.println("gd_Build@initBuild 1, up since: "+core.getUptimeInSeconds() + "s");

        moduleManager.addModule(console, 0);
        moduleManager.addModule(cameraManager, 1);
        moduleManager.addModule(soundGenerator, 3);
        moduleManager.addModule(particleEffects, 4);
        //moduleManager.addModule(mm, 6);
        moduleManager.addModule(soundManager, 7);


        stateManager.setCurrentState(game);
        moduleManager.initModules(this);
        stateManager.init(this);


        System.out.println("gd_Build@initBuild 2, up since: "+core.getUptimeInSeconds() + "s");

    }

    public void update(){

        input.takeInput(this);

        moduleManager.preUpdate(this);
        objectManager.preUpdate(this);
        stateManager.preUpdate(this);

        stateManager.update(this);
        moduleManager.update(this);
        objectManager.update(this);

        stateManager.postUpdate(this);
        objectManager.postUpdate(this);
        moduleManager.postUpdate(this);

        output.getRenderer().update();

    }

    public void draw(){

        output.getRenderer().startDraw(this);

        //graphics
        output.getRenderer().setUpGraphics(this);

        // make render and update ranks ? -earlier +later 0 good.states
        //module manager has to draw first atm because of camera translation, this also kinda brakes all the other
        //modules that may draw before it
        //or have a pre draw thingy

        moduleManager.preDraw(output.getRenderer());
        objectManager.preDraw(output.getRenderer());
        stateManager.preDraw(output.getRenderer());

        moduleManager.draw(output.getRenderer());
        objectManager.draw(output.getRenderer());
        stateManager.draw(output.getRenderer());

        stateManager.postDraw(output.getRenderer());
        objectManager.postDraw(output.getRenderer());
        moduleManager.postDraw(output.getRenderer());

        output.getRenderer().endGraphics(this);

        //hud
        output.getRenderer().setUpHudGraphics(this);

        stateManager.drawHud(output.getRenderer());
        moduleManager.drawHud(output.getRenderer());

        output.getRenderer().endHudGraphics(this);

        output.getRenderer().endDraw(this);

    }

    public void takeInput(gd_Input input){

        moduleManager.takeInput(input);
        objectManager.takeInput(input);
        stateManager.takeInput(input);

    }

    public void takePolledInput(gd_Input input){

        moduleManager.takePolledInput(input);
        objectManager.takePolledInput(input);
        stateManager.takePolledInput(input);

    }

    public gd_ObjectManager getObjectManager() {
        return objectManager;
    }

    public gd_StateManager getStateManager() {
        return stateManager;
    }

    public gd_ModuleManager getModuleManager() {
        return moduleManager;
    }

    public gd_Output getOutput() {
        return output;
    }


    public void toggleWireframe(){
        // this should not be here
        gd_Config.wireframe = !gd_Config.wireframe;
    }

    public String setFps(String val){
        // this should not be here

        //System.out.println("Value:::");

        //System.out.println(val);

        int value;

        try {
            value = Integer.valueOf(val);
        }catch (Exception e){

            return "Please enter a valid int >0";
            //e.printStackTrace();
        }

        if(value > 0){
            gd_Config.maxFps = value;
            return "FPS set to: "+value;
        }

        return "Please enter a valid int >0";

    }

    public String drawFps(String val){
        // this should not be here

        //System.out.println("Value:::");

        //System.out.println(val);

        boolean value;

        try {
            value = Boolean.valueOf(val);
        }catch (Exception e){

            return "Please enter a valid boolean";
            //e.printStackTrace();
        }

        gd_Config.drawFps = value;

        return "FPS shown: "+value;

    }

    public void release(){
    }

    public void terminate(){
        System.out.println("terminate build!");
        gd_output_Renderer.closeWindow();
    }

    public float getUptimeInSeconds() {
        return core.getUptimeInSeconds();
    }

}

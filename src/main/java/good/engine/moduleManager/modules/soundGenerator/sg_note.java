package good.engine.moduleManager.modules.soundGenerator;

/**
 * Created by Valentin on 05/08/2015.
 */
public class sg_note {

    public static enum types {SIXTEENS, EIGHTHS, QUARTER, HALF, FULL}

    private types type = types.EIGHTHS;

    private float volume = 1; //0-1
    private int octave; //1-8
    private int note; //1-12
    private sg_instrument Instrument;
    private int delay = 0; // 1-120
    private boolean blank = false;

    private float stereo = 0; //-1 - 1

    public sg_note(types type, int note, int octave, float volume, float stereo, sg_instrument Instrument){

        this.type = type;


        if(note >= 1 && note <= 12) this.note = note;
        else this.note = 1;

        if(octave >= 1 && octave <= 10) this.octave = octave;
        else this.octave = 1;

        if(stereo >= -1 && stereo <= 1) this.stereo = stereo;
        else this.stereo = 0;

        if(volume >= 0 && volume <= 1) this.volume = volume;
        else this.stereo = 0;

        this.Instrument = Instrument;

    }

    /*public sg_note(types type, int note, int octave, float stereo, sg_instrument Instrument){

        this.type = type;

        if(note >= 1 && note <= 12) this.note = note;
        else this.note = 1;

        if(octave >= 1 && octave <= 10) this.octave = octave;
        else this.octave = 1;

        if(stereo >= -1 && stereo <= 1) this.stereo = stereo;
        else this.stereo = 0;

        this.Instrument = Instrument;

    }*/

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public sg_instrument getInstrument() {
        return Instrument;
    }

    public void setInstrument(sg_instrument Instrument) {
        this.Instrument = Instrument;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public int getOctave() {
        return octave;
    }

    public void setBlank() { blank = true; }

    public boolean getBlank() { return blank; }

    public float getStereo() {
        return stereo;
    }

    public types getType() {
        return type;
    }

    public float getVolume() {
        return volume;
    }

}

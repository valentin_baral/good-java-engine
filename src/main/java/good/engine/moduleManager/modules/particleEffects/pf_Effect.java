package good.engine.moduleManager.modules.particleEffects;

import good.engine.helper.gd_Math;
import good.engine.main.gd_Config;
import org.lwjgl.util.vector.Vector3f;

public class pf_Effect {

    public Vector3f changePerSecond;
    public Vector3f randomChangePerSecond;
    public Vector3f changeDirectionChance; // 0-1 = likeliness
    public Vector3f changeDirectionChanceModifier; // 0-1 = likeliness of which direction

    public pf_Effect() {
        this(new Vector3f(), new Vector3f(), new Vector3f(), new Vector3f());
    }

    public pf_Effect(Vector3f changePerSecond) {
        this(changePerSecond, new Vector3f(), new Vector3f(), new Vector3f());
    }

    public pf_Effect(Vector3f changePerSecond, Vector3f randomChangePerSecond) {
        this(changePerSecond, randomChangePerSecond, new Vector3f(), new Vector3f());
    }

    public pf_Effect(Vector3f changePerSecond, Vector3f randomChangePerSecond, Vector3f changeDirectionChance) {
        this(changePerSecond, randomChangePerSecond, changeDirectionChance, new Vector3f());
    }

    public pf_Effect(Vector3f changePerSecond, Vector3f randomChangePerSecond, Vector3f changeDirectionChance, Vector3f changeDirectionChanceModifier) {
        this.changePerSecond = changePerSecond;
        this.randomChangePerSecond = randomChangePerSecond;
        this.changeDirectionChance = changeDirectionChance;
        this.changeDirectionChanceModifier = changeDirectionChanceModifier;
    }

    public void execute(pf_ParticleEffects effects, int index) {
        effects.positionX.set(index, effects.positionX.get(index) + ((changePerSecond.x + (gd_Math.random.nextInt(Math.round(Math.abs(randomChangePerSecond.x) * 1000) + 1)/1000) * Math.signum(randomChangePerSecond.x)) * effects.directionX.get(index)) * gd_Config.hardDeltaTime());
        effects.positionY.set(index, effects.positionY.get(index) + ((changePerSecond.y + (gd_Math.random.nextInt(Math.round(Math.abs(randomChangePerSecond.y) * 1000) + 1)/1000) * Math.signum(randomChangePerSecond.y)) * effects.directionY.get(index)) * gd_Config.hardDeltaTime());
        effects.positionZ.set(index, effects.positionZ.get(index) + ((changePerSecond.z + (gd_Math.random.nextInt(Math.round(Math.abs(randomChangePerSecond.z) * 1000) + 1)/1000) * Math.signum(randomChangePerSecond.z)) * effects.directionZ.get(index)) * gd_Config.hardDeltaTime());

        float chanceX = changeDirectionChance.x;
        if (changeDirectionChanceModifier.x != 0) {
            if (Math.signum(changeDirectionChanceModifier.x) == effects.directionX.get(index)) {
                // less likely to change

                chanceX = chanceX * Math.abs(changeDirectionChanceModifier.x);
            } else {
                // more likely to change

                chanceX = chanceX * (1f / (1f-Math.abs(changeDirectionChanceModifier.x)));
            }
        }
        if (changeDirectionChance.x > 0 && gd_Math.random.nextFloat() <= chanceX) {
            effects.directionX.set(index, effects.directionX.get(index) * -1);
        }

        float chanceY = changeDirectionChance.y;
        if (changeDirectionChanceModifier.y != 0) {
            if (Math.signum(changeDirectionChanceModifier.y) == effects.directionY.get(index)) {
                // less likely to change

                chanceY = chanceY * Math.abs(changeDirectionChanceModifier.y);
            } else {
                // more likely to change

                chanceY = chanceY * (1f / (1f-Math.abs(changeDirectionChanceModifier.y)));
            }
        }
        if (changeDirectionChance.y > 0 && gd_Math.random.nextFloat() <= chanceY) {
            effects.directionY.set(index, effects.directionY.get(index) * -1);
        }

        float chanceZ = changeDirectionChance.z;
        if (changeDirectionChanceModifier.z != 0) {
            if (Math.signum(changeDirectionChanceModifier.z) == effects.directionZ.get(index)) {
                // less likely to change

                chanceZ = chanceZ * Math.abs(changeDirectionChanceModifier.z);
            } else {
                // more likely to change

                chanceZ = chanceZ * (1f / (1f-Math.abs(changeDirectionChanceModifier.z)));
            }
        }
        if (changeDirectionChance.z > 0 && gd_Math.random.nextFloat() <= chanceZ) {
            effects.directionZ.set(index, effects.directionZ.get(index) * -1);
        }
    }

}

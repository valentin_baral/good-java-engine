package good.engine.output.audio;

import good.engine.main.gd_Config;
import org.newdawn.slick.util.ResourceLoader;

import javax.sound.sampled.*;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by valentinbaral on 03/11/2015.
 */
public class gd_output_AudioLine {


    private static AudioFormat audioFormat;
    private static AudioInputStream audioInputStream;
    private static SourceDataLine sourceDataLine;

    private static byte audioData[];

    //public final static Map<String, Float> NOTES = new HashMap<String, Float>();

    private static boolean SIGNED = true;
    //Allowable true,false
    private static boolean BIG_ENDIAN = false;
    //Allowable true,false
    public static final int CHANNELS = 2;
    public static final int SAMPLE_RATE = 44100;
    public static final int BIT = 16;
    public static final float speed = 1f;
    //public static final int NUM_PRODUCERS = 10;
    //public static final int BUFFER_SIZE_FRAMES = (int)(SAMPLE_RATE*speed);
    public static final int BUFFER_SIZE_FRAMES = (int)(SAMPLE_RATE*speed)/16; // 8th


    private static final short[] mixBuffer = new short[BUFFER_SIZE_FRAMES * CHANNELS];
    private static final byte[] audioBuffer = new byte[BUFFER_SIZE_FRAMES * CHANNELS * 2];

    public static double MIXDOWN_VOLUME = 0.5;

    private static final AtomicBoolean running = new AtomicBoolean(true);
    public static final AtomicLong position = new AtomicLong();
    public static final AtomicLong bufferPosition = new AtomicLong();

    //private static final double MIXDOWN_VOLUME = 1.0 / NUM_PRODUCERS;

    private static SourceDataLine mainLine = null;

    // thread that play all the sounds
    private static Thread output;

    public static int currentpos = 0;

    private static ArrayList<QueuedBlock> input = new ArrayList<QueuedBlock>();
    private static ArrayList<QueuedBlock> finished = new ArrayList<QueuedBlock>();
    //private static ArrayList<Long> schedule = new ArrayList<Long>();

    public gd_output_AudioLine(){
        start();
    }

    public void update() {

    }

    public void start(){
        output = new output();
        output.start();
    }

    // audio block with "when to play" tag
    private static class QueuedBlock {

        long when;
        short[] data;

        public QueuedBlock(long when, short[] data) {
            this.when = when;
            this.data = data;
        }
    }

    public static void mix(long when, short[] data) {
        input.add(new QueuedBlock(when, Arrays.copyOf(data, data.length)));
    }

    public static short[] loadWavAudioFile(String url) {

        try {

            ByteArrayOutputStream out = new ByteArrayOutputStream();

            BufferedInputStream in = new BufferedInputStream(ResourceLoader.getResourceAsStream(url));

            AudioInputStream sound = AudioSystem.getAudioInputStream(in);

            int read;
            byte[] buff = new byte[1024];
            while ((read = sound.read(buff)) > 0) {
                out.write(buff, 0, read);
            }
            out.flush();
            byte[] audioBytes = out.toByteArray();

            short[] shorts = new short[audioBytes.length/2];

            for (int i = 0; i <audioBytes.length/2 ; i++)
            {
                shorts[i] = ( (short)( ( audioBytes[i*2] & 0xff )|( audioBytes[i*2 + 1] << 8 ) ) );
            }

            //ShortBuffer shortBuffer = ByteBuffer.wrap(audioBytes).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer();
            return shorts;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new short[0];

    }

    public static long getPosition(){

        if(mainLine == null) return 0;

        return mainLine.getLongFramePosition();

    }

    // mix down single buffer and offer to the audio device
    private static void processSingleBuffer(SourceDataLine line) {

        Arrays.fill(mixBuffer, (short) 0);
        long bufferStartAt = position.get();

        //System.out.println("@processSingleBuffer = "+System.currentTimeMillis());

        // mixdown audio blocks
        for (int i=0; i<input.size(); i++) {

            //System.out.println("mix s= "+input.size());

            QueuedBlock comp = input.get(i);

            int blockFrames = comp.data.length / CHANNELS;

            //System.out.println("blockFrames= "+blockFrames);

            //System.out.println("when= "+comp.when);

            // block fully played - mark for deletion
            if (comp.when + blockFrames <= bufferStartAt) {
                finished.add(comp);
                continue;
            }

            // block starts after end of current buffer
            if (bufferStartAt + BUFFER_SIZE_FRAMES <= comp.when)
                continue;

            // mix in part of the block which overlaps current buffer
            // note that block may have already started in the past
            // but extends into the current buffer, or that it starts
            // in the future but before the end of the current buffer
            int blockOffset = Math.max(0, (int) (bufferStartAt - comp.when));
            int blockMaxFrames = blockFrames - blockOffset;
            int bufferOffset = Math.max(0, (int) (comp.when - bufferStartAt));
            int bufferMaxFrames = BUFFER_SIZE_FRAMES - bufferOffset;

            //System.out.println("blockOffset= "+blockOffset);
            //System.out.println("blockMaxFrames= "+blockMaxFrames);
            //System.out.println("bufferOffset= "+bufferOffset);
            //System.out.println("bufferMaxFrames= "+bufferMaxFrames);


            for (int f = 0; f < blockMaxFrames && f < bufferMaxFrames; f++) {
                for (int c = 0; c < CHANNELS; c++) {
                    int bufferIndex = (bufferOffset + f) * CHANNELS + c;
                    int blockIndex = (blockOffset + f) * CHANNELS + c;

                    short newData = (short) (comp.data[blockIndex] * MIXDOWN_VOLUME * gd_Config.volume);

                    mixBuffer[bufferIndex] += newData;

                    //if(newData > mixBuffer[bufferIndex]) mixBuffer[bufferIndex] = newData;

                    //if(mixBuffer[bufferIndex] > 100) mixBuffer[bufferIndex] = 100;

                }
            }
        }

        input.removeAll(finished);

        /*for(int i=0; i<finished.size(); i++){
            finished.get(i).origin.setPlayed(true);
        }*/

        finished.clear();

        //normalize(mixBuffer);

        ByteBuffer.wrap(audioBuffer).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().put(mixBuffer);
        line.write(audioBuffer, 0, audioBuffer.length);
        position.addAndGet(BUFFER_SIZE_FRAMES);
    }

    public static void clean() {
        input.clear();
    }

    public static void normalize(short[] mixBuffer) {

        short min = Short.MAX_VALUE;
        short max = Short.MIN_VALUE;
        for (int i = 0; i < mixBuffer.length; i++) {
            short value = mixBuffer[i];
            min = (short) Math.min(min, value);
            max = (short) Math.max(max, value);
        }

        for (int i = 0; i < mixBuffer.length; i++) {

            short value = mixBuffer[i];

            short targetMax = 12000;

            //This is the maximum volume reduction
            double maxReduce = 1 - targetMax / (double) max;

            int abs = Math.abs(value);
            double factor = (maxReduce * abs / (double) max);

            mixBuffer[i] = (short) Math.round((1 - factor) * value);

        }

    }

    class output extends Thread{
        //This is a working buffer used to transfer
        // the data between the AudioInputStream and
        // the SourceDataLine.  The size is rather
        // arbitrary.
        byte playBuffer[] = new byte[SAMPLE_RATE];

        public void run() {

            //System.out.println("ruuuuuuuuuun");

            Mixer.Info[] mixerInfo = AudioSystem.getMixerInfo();
            Mixer mixer = AudioSystem.getMixer(mixerInfo[0]);

            try {

                Line.Info[] lineInfo = mixer.getSourceLineInfo();
                mainLine = (SourceDataLine) mixer.getLine(lineInfo[0]);

                mainLine.open(new AudioFormat(SAMPLE_RATE, BIT, CHANNELS, SIGNED, BIG_ENDIAN), BUFFER_SIZE_FRAMES);
                mainLine.start();

                while (running.get()) {
                    processSingleBuffer(mainLine);
                }

                mainLine.stop();

            } catch (Throwable t) {
                //System.out.println(t.getMessage());
                t.printStackTrace();
            }
        }

    }



}

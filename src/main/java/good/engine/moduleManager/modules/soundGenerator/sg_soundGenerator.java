package good.engine.moduleManager.modules.soundGenerator;

import good.engine.input.gd_Input;
import good.engine.main.gd_Build;
import good.engine.moduleManager.modules.console.cs_Command;
import good.engine.moduleManager.modules.console.cs_Console;
import good.engine.moduleManager.modules.console.cs_Package;
import good.engine.moduleManager.gd_Module;
import good.engine.output.audio.gd_output_AudioLine;
import good.engine.output.render.gd_output_Renderer;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;

/**
 * Created by Valentin on 05/08/2015.
 */
@Singleton
public class sg_soundGenerator extends gd_Module {

    /*

        would be better if I would create virtual instruments instead
        and then call them to play a note (each createing small build thread, then feed to playback thread?)

     */

    public final static float[] NOTES = {16.35f,17.32f,18.35f,19.45f,20.60f,21.83f,23.12f,24.50f,25.96f,27.50f,29.14f,30.87f};
    public final static int[] OCTAVES = {1,2,4,8,16,32,64,128};

    //private ArrayList<sg_channelType> requests = new ArrayList<sg_channelType>();

    private ArrayList<sg_rule> rules = new ArrayList<sg_rule>();

    private ArrayList<sg_track> tracks = new ArrayList<sg_track>();

    private sg_track currentTrack = null;

    //private int[] values = new int[120];


    cs_Console console;

    @Inject
    public sg_soundGenerator(cs_Console console) {
        this.console = console;
    }

    public void init(gd_Build engine){

        cs_Package Package = new cs_Package("sound", this, "good.good.engine.moduleManager.modules.soundGenerator.sg_sound_generator");
        Package.addCommand(new cs_Command("volume", "cs_f_setVolume", true));
        Package.addCommand(new cs_Command("mute", "cs_f_toggleMute", false));
        Package.addCommand(new cs_Command("start", "cs_f_startPlayback", false));
        Package.addCommand(new cs_Command("stop", "cs_f_stopPlayback", false));
        Package.addCommand(new cs_Command("pause", "cs_f_pausePlayback", false));

        console.addPackage(Package);

        //Arrays.fill(values, 0);

    }

    public void takeInput(gd_Input input){

    }

    public void takePolledInput(gd_Input input){

    }

    public void update(gd_Build engine){

        if(currentTrack != null){

            updateCurrentTrack();


        }

        //for(int x = 0; x<values.length-1; x++) { values[x] = values[x+1]; }

        //values[values.length-1] = gd_output_audioLine.currentpos;

        //play();


    }

    public void draw(gd_output_Renderer renderer){

    }

    public void drawHud(gd_output_Renderer renderer){

    }

    public void play(){

    }

    private void updateCurrentTrack(){

        currentTrack.update();

        /*if(leadQueue.size()<3){
            genComponent(lines.LEAD);
        }

        if(mainQueue.size()<3){
            genComponent(lines.MAIN);
        }

        if(backQueue.size()<3){
            genComponent(lines.BACK);
        }*/

    }

    public ArrayList<sg_track> getTracks() {
        return tracks;
    }

    public void addTrack(sg_track track){
        System.out.println("@addTrack");

        tracks.add(track);

        if(currentTrack == null){
            System.out.println("@currentTrack");
            currentTrack = track;
        }
    }

    public String cs_f_setVolume(String val){

        //System.out.println("Value:::");

        //System.out.println(val);

        float value;

        try {
            value = Float.valueOf(val);
        }catch (Exception e){

            return "Please enter a valid float 0.0-1.0";
            //e.printStackTrace();
        }

        //System.out.println(val);


        //if(Float.isNaN(value)) return "Please enter a valid float 0.0-1.0";

        //System.out.println("Value:::");
        //System.out.println(value);

        if(value >= 0 && value <= 1 ){
            gd_output_AudioLine.MIXDOWN_VOLUME = value;
            return "Volume set to: "+value;
        }

        return "Please enter a valid float 0.0-1.0";

    }

}

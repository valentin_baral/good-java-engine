package good.engine.moduleManager.modules.cameraManager;

import good.engine.input.gd_Input;
import good.engine.main.gd_Build;
import good.engine.main.gd_Config;
import good.engine.moduleManager.modules.console.cs_Command;
import good.engine.moduleManager.modules.console.cs_Console;
import good.engine.moduleManager.modules.console.cs_Package;
import good.engine.moduleManager.gd_Module;
import good.engine.output.render.gd_output_Renderer;
import org.lwjgl.input.Mouse;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;

/**
 * Created by Valentin on 13/11/2015.
 */
@Singleton
public class gd_CameraManager extends gd_Module {

    public boolean debug = false;

    private int count = 0;

    private ArrayList<cm_Camera> Cameras = new ArrayList<>();

    private cm_Camera activeCamera = null;

    cs_Console console;

    @Inject
    public gd_CameraManager(cs_Console console) {
        this.console = console;
    }


    public void init(gd_Build engine) {


        if(console != null){

            cs_Console cs = (cs_Console) console;

            cs_Package consolePackage = new cs_Package("cameramanager", this, "good.good.engine.moduleManager.modules.cameraManager");
            consolePackage.addCommand(new cs_Command("fov", "cs_f_setFieldOfView", true));

            cs.addPackage(consolePackage);

        }

    }

    public void takeInput(gd_Input input) {

        if(activeCamera == null) return;

        if(activeCamera.isMouseControlled()){

            if(Mouse.isGrabbed()){

                float sensitivity = gd_Config.sensitivity;

                float mouseDX = Mouse.getDX() * 0.025f * sensitivity;
                float mouseDY = Mouse.getDY() * 0.025f * sensitivity;

                if (activeCamera.getRotation().y + mouseDX >= 360) {
                    activeCamera.getRotation().y = activeCamera.getRotation().y + mouseDX - 360;
                } else if (activeCamera.getRotation().y + mouseDX < 0) {
                    activeCamera.getRotation().y = 360 - activeCamera.getRotation().y + mouseDX;
                } else {
                    activeCamera.getRotation().y += mouseDX;
                }
                if (activeCamera.getRotation().x - mouseDY >= -89 && activeCamera.getRotation().x - mouseDY <= 89) {
                    activeCamera.getRotation().x += -mouseDY;
                } else if (activeCamera.getRotation().x - mouseDY < -89) {
                    activeCamera.getRotation().x = -89;
                } else if (activeCamera.getRotation().x - mouseDY > 89) {
                    activeCamera.getRotation().x = 89;
                }

                if(debug) {
                    System.out.println("mouseDX: " + mouseDX);
                    System.out.println("mouseDY: " + mouseDY);
                }

            }

        }

    }

    public void takePolledInput(gd_Input input) {

    }

    // maybe pre update would be better?
    public void postUpdate(gd_Build engine) {
        if(activeCamera != null) {
            engine.getOutput().getRenderer().getCamera().setPosition(activeCamera.getPosition());
            engine.getOutput().getRenderer().getCamera().setRotation(activeCamera.getRotation());
            engine.getOutput().getRenderer().getCamera().setFieldOfView(activeCamera.getFieldOfView());
        }
    }

    public void preDraw(gd_output_Renderer renderer){

    }

    public void draw(gd_output_Renderer renderer) {}

    public void drawHud(gd_output_Renderer renderer) {

    }

    public int addCamera(cm_Camera camera){

        count++;

        camera.setId(count);
        Cameras.add(camera);

        return count;

    }

    public cm_Camera getCameraForId(int id){

        for(int c=0; c<Cameras.size(); c++){
            if(Cameras.get(c).getId() == id){
                return Cameras.get(c);
            }
        }

        return null;

    }

    public cm_Camera getActiveCamera(){
        return activeCamera;
    }

    public void setActiveCamera(cm_Camera activeCamera) {
        this.activeCamera = activeCamera;
    }

    public String cs_f_setFieldOfView(String val){

        float value;

        if(activeCamera == null) return "no active camera";

        if(val == "" || val == null || val.length() < 1) return String.valueOf(activeCamera.getFieldOfView());

        try {
            value = Float.valueOf(val);
        }catch (Exception e){

            return "Please enter a valid float 1.0-170.0";
            //e.printStackTrace();
        }

        if(value >= 1f && value <= 170f ){
            activeCamera.setFieldOfView(value);
            return "FOV set to: "+value;
        }

        return "Please enter a valid float 0.0-1.0";

    }

}

package good.engine.entity.graphics;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import good.math.gd_Math;
import org.lwjgl.util.Color;
import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class gd_gfx_Shape extends gd_gfx_Renderer {

    private ArrayList<gd_gfx_Triangle> triangles = new ArrayList<gd_gfx_Triangle>();

    private Vector3f originalPosition = new Vector3f(0,0,0);
    private Vector3f position = new Vector3f(0,0,0);

    private Vector3f originalRotation = new Vector3f(0,0,0);
    private Vector3f rotation = new Vector3f(0,0,0);

    private Vector3f originalScale = new Vector3f(1,1,1);
    private Vector3f scale = new Vector3f(1,1,1);

    // shape will always face to gd_output_camera's rotation
    private boolean billboard = false;

    private gd_gfx_Texture originalTexture = null;
    private gd_gfx_Texture texture = null;
    private boolean flipTextureX = false;
    private boolean flipTextureY = false;

    //private boolean repeatTexture = false;
    // if texture repeat is on, not pixels but ingame size
    //private float textureWidth = 5;
    //private float textureHeight = 5;

    public float red = 10;
    public float green = 10;
    public float blue = 10;

    public boolean ignoreLighting = false;


    public gd_gfx_Shape(){}

    public ArrayList<gd_gfx_Triangle> getTriangles() {
        return triangles;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        this.position.set(position);
        originalPosition.set(position);
    }

    public Vector3f getOriginalPosition() {
        return originalPosition;
    }

    public void setOriginalPosition(Vector3f originalPosition) {
        this.originalPosition.set(originalPosition);
    }

    public gd_gfx_Texture getTexture() {
        return texture;
    }

    public void setTexture(gd_gfx_Texture texture) {
        setTexture(texture, true);
    }

    public void setTexture(gd_gfx_Texture texture, boolean setOriginal) {
        this.texture = texture;
        if (setOriginal) {
            originalTexture = texture;
        }
    }


    /**
     * Calculates the BoundingBox of this Shape. It does not
     * incorporate the shapes position but does incorporate
     * the shapes rotation.
     * @return
     */
    public BoundingBox getRotatetBoundingBox()
    {
        ArrayList<Vector3> points = new ArrayList<>();
        for(int t = 0; t < triangles.size(); t++)
        {
            ArrayList<Vector3f> trianglePoints = triangles.get(t).getPoints();
            for(int p = 0; p < trianglePoints.size(); p++)
            {
                Vector3 point = gd_Math.convertVector(trianglePoints.get(p));
                //point.add(gd_Math.convertVector(position));
                points.add(point);
            }
        }
        //new Quaternion().setf
        BoundingBox bb = new BoundingBox();
        Quaternion rot = new Quaternion().setEulerAngles(getRotation().y, getRotation().x, getRotation().z);
        for(int i = 0; i < points.size(); i++)
        {
            final Vector3 point = points.get(i);
            final Vector3 mul = point.mul(rot);
            mul.scl(gd_Math.convertVector(scale));
            bb.ext(mul);
        }
        return bb;
    }

    public boolean hasTexture(){
        //return false;
        if(texture == null) return false;
        return true;
    }

    public boolean isBillboard() {
        return billboard;
    }

    public void setBillboard(boolean billboard) {
        this.billboard = billboard;
    }

    public Vector3f getRotation() {
        return rotation;
    }

    public void setRotation(Vector3f rotation) {
        setRotation(rotation,true);
    }
    public void setRotation(Vector3 rotation) {
        setRotation(gd_Math.convertVector(rotation),true);
    }

    public void setRotation(Vector3f rotation, boolean setOriginal) {
        setRotation(rotation.x,rotation.y,rotation.z,true);

    }

    public void setRotation(float x, float y, float z, boolean setOriginal) {
        rotation.set(x,y,z);
        if (setOriginal) originalRotation.set(x,y,z);
    }

    public Vector3f getScale() {
        return scale;
    }

    public void setScale(Vector3f scale) {
        this.scale.set(scale);
        originalScale.set(scale);
    }


    public Vector3f getOriginalScale() {
        return originalScale;
    }

    public void setColor(int r, int g, int b){
        red = r;
        green = g;
        blue = b;
    }
    public void setColor(Color c){
        this.setColor(c.getRed(),c.getGreen(),c.getBlue());
    }

    public Vector3f getOriginalRotation() {
        return originalRotation;
    }

    public gd_gfx_Texture getOriginalTexture() {
        return originalTexture;
    }

    public boolean isFlipTextureX() {
        return flipTextureX;
    }

    public void setFlipTextureX(boolean flipTextureX) {
        this.flipTextureX = flipTextureX;
    }

    public boolean isFlipTextureY() {
        return flipTextureY;
    }

    public void setFlipTextureY(boolean flipTextureY) {
        this.flipTextureY = flipTextureY;
    }
    public gd_gfx_Shape clone() {
        return clone(new gd_gfx_Shape());
    }
    public gd_gfx_Shape clone(gd_gfx_Shape newShape) {
        newShape.getTriangles().addAll(triangles);
        newShape.setScale(scale);
        newShape.setFlipTextureX(flipTextureX);
        newShape.setFlipTextureY(flipTextureY);
        newShape.red = red;
        newShape.blue = blue;
        newShape.green = green;
        newShape.setRotation(rotation);
        newShape.setPosition(position);
        newShape.setTexture(texture);
        newShape.setBillboard(billboard);
        return newShape;
    }

    @Override
    public void render() {

    }
}

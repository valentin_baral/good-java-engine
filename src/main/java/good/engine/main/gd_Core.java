package good.engine.main;

import good.states.gd_State;




/**
 * Created by valentinbaral on 02/11/2015.
 */
public class gd_Core {

    public static int FPS = 0;
    public static int UPDATES = 0;

    public long timeStart = 0;
    //Canvas3D myCanvas3D = new Canvas3D(null);

    private boolean running;
    private int frameCount = 0;
    public static int updateCount2 = 0;

    //At the very most we will update the game this many times before a new render.
    //If you're worried about visual hitches more than perfect timing, set this to 1.
    final int MAX_UPDATES_BEFORE_RENDER = 5;
    //We will need the last update time.
    double lastUpdateTime = System.currentTimeMillis();
    //Store the last time we rendered.
    double lastRenderTime = System.currentTimeMillis();

    //Simple way of finding FPS.
    int lastSecondTime = (int) (lastUpdateTime / 1000);

    private gd_Build build;
    public CoreComponent coreInjector;

    gd_State game;

    public gd_Core() {
        coreInjector = DaggerCoreComponent.create();
    }

    public void start(gd_State game){

        this.game = game;
        System.out.println("start");

        timeStart = System.currentTimeMillis();

        running = true;
        run();
    }

    public void run() {
        System.out.println("run");
        try {
            initBuild();
            initLoop();
            build.release();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            // build is null here?
            //build.terminate();

            // TODO: raus wegen debug zwecken
            // /gd_output_Renderer.closeWindow();
        }
    }

    private void initLoop(){
        while(running){
            loop();
        }
    }
    public void initBuild() {
        System.out.println("init Build");
        build = coreInjector.getBuild();
        build.init(this, game);
    }

    private void loop(){

        double TIME_BETWEEN_RENDERS = 1000f / (float)gd_Config.maxFps;
        double TIME_BETWEEN_UPDATES = gd_Config.hardDeltaTime() * 1000f;

        double now = System.currentTimeMillis();
        int updateCount = 0;

        if(true){

            //Do as many game updates as we need to, potentially catchup.
            while( now - lastUpdateTime > TIME_BETWEEN_UPDATES ){
                update();
                lastUpdateTime = lastUpdateTime + TIME_BETWEEN_UPDATES;
                updateCount++;
                updateCount2++;

            }

            if ( now - lastUpdateTime > TIME_BETWEEN_UPDATES){
                lastUpdateTime = now - TIME_BETWEEN_UPDATES;
            }


            if ( now - lastRenderTime > TIME_BETWEEN_RENDERS ) {
                draw();
                lastRenderTime = lastRenderTime + TIME_BETWEEN_RENDERS;
                frameCount++;

            }

            if ( now - lastRenderTime > TIME_BETWEEN_RENDERS){
                lastRenderTime = now - TIME_BETWEEN_RENDERS;
            }

            //Update the frames we got.
            int thisSecond = (int) (lastUpdateTime / 1000f);
            if(thisSecond > lastSecondTime){
                //System.out.println(FPS+" fps");
                //System.out.println(updateCount2+" ups");
                FPS = frameCount;
                UPDATES = updateCount2;
                updateCount2 = 0;
                frameCount = 0;
                lastSecondTime = thisSecond;
            }

            //Yield until it has been at least the target time between renders. This saves the CPU from hogging.
            while( now - lastRenderTime < TIME_BETWEEN_RENDERS && now - lastUpdateTime < TIME_BETWEEN_UPDATES){
                //Thread.yield();

                //This stops the app from consuming all your CPU. It makes this slightly less accurate, but is worth it.
                //You can remove this line and it will still work (better), your CPU just climbs on certain OSes.
                //FYI on some OS's this can cause pretty bad stuttering. Scroll down and have a look at different peoples' solutions to this.
                //try {Thread.sleep(1);} catch(Exception e) {}

                now = System.currentTimeMillis();
            }
        }
    }

    public void stop(){
        if(running){
            running = false;
        }
    }

    private void update() {

        build.update();

    }
    private void draw() {

        build.draw();

    }

    public float getUptimeInSeconds() {
        long now = System.currentTimeMillis();
        long diff = now - timeStart;

        /*System.out.println("now === "+ now);
        System.out.println("start = "+ timeStart);
        System.out.println("diff == "+ diff);*/

        return (diff / (1000f * 60f));
    }

}

package good.engine.moduleManager.modules.soundGenerator;

/**
 * Created by valentinbaral on 03/12/2015.
 */
public class sg_channelType {

    // right now Im comparing instances rather thane titleScreen string this might make problems in the future

    private String name;

    public sg_channelType(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

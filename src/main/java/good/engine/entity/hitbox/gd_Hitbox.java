package good.engine.entity.hitbox;

import org.lwjgl.util.vector.Vector3f;

/**
 * Created by valentinbaral on 17/11/2015.
 */
public class gd_Hitbox {

    protected Vector3f position = new Vector3f(0,0,0);
    private Vector3f size = new Vector3f(0,0,0);
    private boolean isTrigger = false;

    public gd_Hitbox(){

    }

    public gd_Hitbox(Vector3f size) {
        this.size.set(size);
    }

    public void setSize(Vector3f size) {
        this.size.set(size);
    }

    public Vector3f getSize() {
        return size;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        this.position.set(position);
    }

    public boolean isTrigger() {
        return isTrigger;
    }

    public void setTrigger(boolean trigger) {
        isTrigger = trigger;
    }
}

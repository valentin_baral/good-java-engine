package good.engine.moduleManager.modules.soundGenerator;

import good.engine.output.audio.gd_output_AudioLine;

import java.util.ArrayList;

/**
 * Created by Valentin on 24/08/2015.
 */
public class sg_s_synthesizer {

    public static int CHANNELS = 2; //Java allows 1 or 2
    public static float SAMPLE_RATE = 44100; //44100

    public static void play(sg_channel chan, sg_component component) {
        gen(chan, component, gd_output_AudioLine.position.get()+ gd_output_AudioLine.BUFFER_SIZE_FRAMES);
    }

    public static long gen(sg_channel chan, sg_component component, long startTime) {

        long longesttime = 0;
        float speed;

        ArrayList<sg_component> components = component.getAllComponents();

        for(int c=0; c<components.size(); c++) {

            sg_component modcomp = components.get(c);

            int time = 0;

            for (int i = 0; i < modcomp.getNotes().size(); i++) {

                //long noteTime = (startTime/component.getNotes().size())*(i+1);

                sg_note note = modcomp.getNotes().get(i);

                if (!note.getBlank()) {

                    new synth().genNote(note, startTime, time, chan);

                }

                int multi = 0;

                switch (note.getType()) {
                    case SIXTEENS:
                        multi = 1;
                        break;
                    case EIGHTHS:
                        multi = 2;
                        break;
                    case QUARTER:
                        multi = 4;
                        break;
                    case HALF:
                        multi = 8;
                        break;
                    case FULL:
                        multi = 16;
                        break;
                }

                time = time + multi;

                speed = 1 * chan.getSpeed();

                //int realsize = (int)(SAMPLE_RATE*CHANNELS*speed);

                int size = (int) (SAMPLE_RATE * CHANNELS * speed) * multi;

                //totaltime += size;


            }

            long chantime = modcomp.calcLength(chan);

            //System.out.println("chantime= "+chantime);
            //System.out.println("longesttime= "+longesttime);

            if(longesttime < chantime){
                longesttime = chantime;
            }

            //times.add();

        }

        return longesttime;

    }

    static class synth extends Thread{

        sg_channel chan;
        sg_note note;

        sg_instrument instrument;

        private double frequency;
        private double phase;

        private int CHANNELS = 2; //Java allows 1 or 2
        private int bytesPerSamp = 4; //Based on channels
        //private float SAMPLE_RATE = 16000; //44100

        //private float herz = 500.0f;
        private float speed = 1; // sec per note
        private long startTime;
        private int size;

        short[] buffer;

        public void genNote(sg_note note, long startTime, int index, sg_channel chan){
            this.note = note;
            this.chan = chan;

            this.instrument = note.getInstrument();

            speed = speed*chan.getSpeed();

            //int realsize = (int)(SAMPLE_RATE*CHANNELS*speed);

            int samplesize = (int)(SAMPLE_RATE*CHANNELS*speed);

            int size = 0;

            switch(note.getType()){
                case SIXTEENS:
                    size = samplesize;
                    break;
                case EIGHTHS:
                    size = samplesize*2;
                    break;
                case QUARTER:
                    size = samplesize*4;
                    break;
                case HALF:
                    size = samplesize*8;
                    break;
                case FULL:
                    size = samplesize*16;
                    break;
            }

            //size = (int)(SAMPLE_RATE*CHANNELS*speed*1.25);



            //System.out.println("size"+size);

            buffer = new short[size];

            this.startTime = (long)(startTime+(samplesize*index));

            // -1 because of array indexes
            float note_freq = sg_soundGenerator.NOTES[note.getNote()-1];
            float octave_multi = sg_soundGenerator.OCTAVES[note.getOctave()-1];

            frequency = note_freq * octave_multi;

            //System.out.println("PINCHHHH==== "+instrument.getPinch());

            //System.out.println("frequency==== "+frequency);

        /*if(instrument.getPinch() > 0f){
            frequency += (int)(frequency * instrument.getPinch());
        }else if(instrument.getPinch() < 0f){
            frequency -= (int)(frequency * instrument.getPinch());
        }*/

            frequency += (int)(frequency * instrument.getPinch());

            //System.out.println("frequency==== "+frequency);

            this.start();
        }

        public void fill(short[] block) {

            float[] mod = instrument.getMod();
            int breakcount = mod.length;
            long[] breakpoints = new long[breakcount];

            int time = block.length/CHANNELS;
            //System.out.println("ln== "+block.length);
            //System.out.println("time== "+time);

            float[] mods = new float[time];

            /* get timings for breaks */
            for (int i=0; i<breakcount; i++) {

                breakpoints[i] = (long)((time)*((float)i/(float)breakcount));

            }

            int breakpoint = 0;

            for (int i=0; i<time; i++) {

                for(int x=breakpoint; x<breakcount; x++){

                    if(x == breakpoint) continue;

                    if(i > breakpoints[x]){

                        //System.out.println(breakpoints[breakpoint]);

                        breakpoint++;

                    }

                }

                float step = mod[breakpoint];

                if(breakpoint<breakcount-1){

                    float perc = ((float)i-breakpoints[breakpoint])/(breakpoints[breakpoint+1]-breakpoints[breakpoint]);

                    float modmod;

                    if(mod[breakpoint]>mod[breakpoint+1]) {

                        modmod = -((mod[breakpoint] - mod[breakpoint+1]) * perc);

                    }else {

                        modmod = (mod[breakpoint+1] - mod[breakpoint]) * perc;

                    }

                    step = mod[breakpoint] + modmod;
                }

                mods[i] = step;

            }

            breakpoint = 0;

            for (int f = 0; f < time; f++) {

                float smth = 1/SAMPLE_RATE;

                //double vib = 440 + 10 * Math.sin(2 * Math.PI * 6 * phase * smth);
                double vib = 1;

                phase += frequency / (double)SAMPLE_RATE;

                double angle = Math.sin(phase * 2 * Math.PI * vib);



                double sample = mods[f]*note.getVolume();



                float stereo = note.getStereo();
                // putting a value on all channels (R+L)
                for (int c = 0; c < CHANNELS; c++) {

                    double gain;

                    if(CHANNELS == 2) {
                        gain = stereoForChannel(c, stereo, sample);
                    }else{
                        gain = sample;
                    }

                    if((gain*angle) <= 1){
                        block[f * CHANNELS + c] = (short) ( (gain*angle) * Short.MAX_VALUE);
                    }else{
                        block[f * CHANNELS + c] = (short) ( 1 * Short.MAX_VALUE);
                    }


                }

            }



        }

        public double stereoForChannel(int channel, float stereo, double sample){

            //if(stereo == 0) return sample;

            double gain;

            float mod = 0.5f;

            //System.out.println("stereo "+stereo);

            /*

            0 == 0.5 mod on both channels
            0.25 ==     L: 0.5 + 0.5*0.25
                        R: 0.5 - 0.5*0.25

             */

            if(channel == 0){

                //System.out.println("L");

                if(stereo > 0){
                    //mod = (1-stereo)/2;
                    mod = mod + mod * Math.abs(stereo);
                }else{
                    //mod = 0-(stereo+(stereo+1)/2);
                    mod = mod - mod * Math.abs(stereo);
                }

            }else{

                //System.out.println("R");

                if(stereo > 0){
                    //mod = stereo+(1-stereo)/2;
                    mod = mod - mod * Math.abs(stereo);
                }else{
                    //mod = (1-(float)Math.sqrt(stereo*stereo))/2;
                    mod = mod + mod * Math.abs(stereo);
                }

            }

            /*if(channel == 0){

                //System.out.println("L");

                if(stereo > 0){
                    mod = 1;
                }else{
                    mod = 0;
                }

            }else{

                //System.out.println("R");

                if(stereo > 0){
                    mod = 0;
                }else{
                    mod = 1;
                }

            }*/

            //System.out.println("channel= "+channel);

            //System.out.println("gainmod= "+mod);

            gain = sample * mod;

            return gain;

        }



        public void run(){
            try{
                //System.out.println("buffer start = "+System.currentTimeMillis());
                // only like 3 ns
                fill(buffer);
                //System.out.println("buffer end = "+System.currentTimeMillis());
                gd_output_AudioLine.mix(startTime, buffer);

            }catch (Exception e) {
                e.printStackTrace();
                System.exit(0);
            }

        }
    }

}




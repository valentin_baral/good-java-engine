package good.engine.moduleManager.modules.SoundManager;

import good.engine.input.gd_Input;
import good.engine.main.gd_Build;
import good.engine.moduleManager.gd_Module;
import good.engine.moduleManager.modules.console.cs_Console;
import good.engine.output.audio.gd_output_AudioLine;
import good.engine.output.render.gd_output_Renderer;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;

/**
 * Created by valentinbaral on 24/11/2017.
 */
@Singleton
public class gd_SoundManager extends gd_Module {

    private gd_Build engine = null;

    private ArrayList<sm_Sound> sounds = new ArrayList<>();

    cs_Console console;


    @Inject
    public gd_SoundManager() {setName("soundManager");}


    public void init(gd_Build engine) {

        this.engine = engine;
        if (console != null) {

            /*cs_Console cs = (cs_Console) console;
            cs_Package consolePackage = new cs_Package("map", this, "good.good.states.game.MapManager");
            consolePackage.addCommand(new cs_Command("canAct", "executeAction", true));
            consolePackage.setCore(true);
            cs.addPackage(consolePackage);*/

        }

    }

    public void takeInput(gd_Input input){}

    public void takePolledInput(gd_Input input){

    }

    public void preUpdate(gd_Build engine){

    }

    public void update(gd_Build engine){

        for (int s=0; s<sounds.size(); s++) {
            sm_Sound sound = sounds.get(s);
            if (sound.isPlaying()) {

                if (sound.getDidStart() + (sound.getAudioData().length/2) <= gd_output_AudioLine.position.get() + gd_output_AudioLine.BUFFER_SIZE_FRAMES) {

                    sound.setPlaying(false);

                    if (sound.isLoop()) {
                        playSound(sound);
                    }

                }

            }
        }

    }

    public void postUpdate(gd_Build engine){



    }

    public void draw(gd_output_Renderer renderer){



    }

    public void drawHud(gd_output_Renderer renderer){



    }

    public void clean() {
        for (int s=0; s<sounds.size(); s++) {
            sounds.get(s).setPlaying(false);
        }
        gd_output_AudioLine.clean();
    }

    public void playSound(sm_Sound sound){

        playSound(sound, gd_output_AudioLine.position.get() + gd_output_AudioLine.BUFFER_SIZE_FRAMES);

    }

    public void playSound(sm_Sound sound, long when){

        // just use playing for loop
        sound.setPlaying(true);
        sound.setDidStart(when);
        gd_output_AudioLine.mix(when, sound.getAudioData());

    }

    public sm_Sound loadSoundFile(String name, String url) {

        sm_Sound sound = getSoundForName(name);

        if (sound != null) return sound;
        System.out.println("loading new sound "+name);
        short[] audioData = gd_output_AudioLine.loadWavAudioFile(url);
        sound = new sm_Sound(name, audioData);
        sounds.add(sound);

        return sound;
    }

    public gd_Build getEngine() {
        return engine;
    }

    public void addSound(sm_Sound sound) {
        sounds.add(sound);
    }

    public sm_Sound getSoundForName(String name) {
        for (int s=0; s<sounds.size(); s++) {
            if (sounds.get(s).getName().toUpperCase().equals(name.toUpperCase())) {
                return sounds.get(s);
            }
        }
        return null;
    }

    public void playSoundForName(String name) {
        sm_Sound sound = getSoundForName(name);
        if (sound != null) {
            playSound(sound);
        }

    }

}

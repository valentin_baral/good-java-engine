package good.engine.helper;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by valentinbaral on 03/12/2015.
 */
public class gd_Math {

    public static Random random = new Random();

    public static float calcAngle(float x1, float y1, float x2, float y2){
        float f1 = (float)Math.toDegrees( Math.atan2( y1-y2, x1-x2 ) );
        f1+=270;
        while ( f1 > 360 ) f1-=360;
        while ( f1 <   0 ) f1+=360;
        return f1;
    }

    public static float calcDistance2D(Vector2f p1, Vector2f p2){
        return calcDistance2D(p1.x,p1.y,p2.x,p2.y);
    }

    public static float calcDistance2D(float x1, float y1, float x2, float y2){
        return (float)Math.sqrt((x2-x1) * (x2-x1) + (y2-y1) * (y2-y1));
    }

    public static float calcDistance3D(Vector3f p1, Vector3f p2){
        return calcDistance3D(p1.x, p1.y, p1.z, p2.x,p2.y, p2.z);
    }

    public static float calcDistance3D(float x1, float y1, float z1, float x2, float y2, float z2){
        return (float)Math.sqrt((x2-x1) * (x2-x1) + (y2-y1) * (y2-y1) + (z2-z1) * (z2-z1));
    }

    public static double mRound(double value, double factor) {
        return Math.round(value / factor) * factor;
    }

    public static boolean isInteger(String s, int radix) {
        Scanner sc = new Scanner(s.trim());
        if(!sc.hasNextInt(radix)) return false;
        // we know it starts with a valid int, now make sure
        // there's nothing left!
        sc.nextInt(radix);
        return !sc.hasNext();
    }

}

package good.engine.entity.graphics;


import com.badlogic.gdx.math.Vector3;

/**
 * Created by valentinbaral on 15/11/2015.
 */

public class gd_gfx_BillboardShape extends gd_gfx_Shape{
    @Override
    public boolean isBillboard() {
        return true;
    }

    public Vector3 pivot = null;

    public Vector3 updatedNormal;

    @Override
    public void render() {

    }
}

package good.engine.main;

import dagger.Component;
import good.engine.gd_BasicResources;
import good.engine.moduleManager.gd_ModuleManager;
import good.engine.moduleManager.modules.SoundManager.gd_SoundManager;
import good.engine.moduleManager.modules.cameraManager.gd_CameraManager;
import good.engine.moduleManager.modules.console.cs_Console;
import good.engine.moduleManager.modules.particleEffects.pf_ParticleEffects;
import good.engine.moduleManager.modules.soundGenerator.sg_soundGenerator;

import javax.inject.Singleton;

@Singleton
@Component(
        modules={
                gd_ModuleManager.class
        })
public interface CoreComponent {
    gd_Build getBuild();

    pf_ParticleEffects getParticles();
    gd_SoundManager getSoundManager();
    cs_Console getConsole();
    gd_CameraManager getCameraManaget();
    sg_soundGenerator getSoundGenerator();
    gd_BasicResources getResources();

}

package good.engine.moduleManager.modules.console;

//import com.sun.xml.internal.ws.api.ResourceLoader;
//import sun.font.TrueTypeFont;

import good.engine.input.gd_Input;
import good.engine.main.gd_Build;
import good.engine.moduleManager.gd_Module;
import good.engine.output.render.gd_output_Renderer;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Arrays;

/*
import com.neet.Main.GamePanel;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.*;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.util.ResourceLoader;
import org.newdawn.slick.Color;

import static org.lwjgl.opengl.GL11.glNormal3d;*/

//import org.newdawn.slick.font.effects.ColorEffect;


/**
 * Created by valentinbaral on 24/10/2015.
 */
@Singleton
public class cs_Console extends gd_Module {

    private ArrayList<cs_Package> packages = new ArrayList<cs_Package>();

    private boolean active = false;

    private cs_Input input;

    private boolean antiAlias = true;

    private int lineHeight = 18;
    private int height = 300;

    private int lineOffset = 0;

    private Color baseFontColor = Color.green;

    private int counter = 0;

    @Inject
    public cs_Console(){
        System.out.println("intisalising console");
    }

    public void init(gd_Build engine){

        input = new cs_Input(this);

        /*cs_Package consolePackage = new cs_Package("console", this, "good.good.engine.moduleManager.modules.console.cs_console");
        consolePackage.addCommand(new cs_Command("say", "sayString", true));
        consolePackage.addCommand(new cs_Command("help", "sayString", true));

        consolePackage.setCore(true);

        addPackage(consolePackage);*/


        cs_Package enginePackage = new cs_Package("good/good.engine", engine, "good.good.engine.main.gd_build");
        enginePackage.addCommand(new cs_Command("setfps", "setFps", true));
        enginePackage.addCommand(new cs_Command("wireframe", "toggleWireframe", false));
        enginePackage.addCommand(new cs_Command("drawfps", "drawFps", true));

        addPackage(enginePackage);


    }

    public void takePolledInput(gd_Input input) {

        if (Keyboard.getEventKeyState()) {

            if (Keyboard.getEventKey() == Keyboard.KEY_GRAVE) {
                active = !active;
            }

            this.input.takeInput(input);

        }

    }

    public void takeInput(gd_Input input) {

        if (!isActive()) return;

        if (input.getdWheel() > 0 || input.getdWheel() < 0) {
            lineOffset+=Math.signum(input.getdWheel());
            if (lineOffset < 0) lineOffset = 0;
        }

    }


    public void update(gd_Build engine){

        counter++;

    }

    public void draw(gd_output_Renderer renderer){

    }

    public void drawHud(gd_output_Renderer renderer){

        if(!isActive()) return;

        //renderer.drawRect(gd_config.WIDTH/2, height/2, gd_config.WIDTH, height, 0, 0, 0, 1);

        int maxLinesOnScreen = height / lineHeight - 3;

        if (input.getHistory().size() > 0) {

            for (int i = 0; i < maxLinesOnScreen-1; i++) {

                int index = input.getHistory().size() - lineOffset - 1 - i;

                if (index < 0) continue;
                if (index >= input.getHistory().size()) continue;

                cs_Line line = input.getHistory().get(index);

                renderer.drawFont(50, (maxLinesOnScreen * lineHeight) - ((i * lineHeight)) + 5, line.getText().toUpperCase(), line.getColor());

            }

        }

        String inputLine = input.getCurrentInput().toUpperCase();

        // do this prior to end underscore
        int inputWidth = renderer.getTextWidth(inputLine);

        if(counter >= 60) {
            inputLine += "_";
            if(counter > 120) {
                counter = 0;
            }
        }

        // draw user input
        renderer.drawFont(50, (height - lineHeight * 2), inputLine, baseFontColor);

        // draw hint
        if (input.getGuessedInput() != null) {
            String remainingLetters = "";
            if (input.getGuessedInput().length() > input.getEndInput().length()) {
                remainingLetters = input.getGuessedInput().toUpperCase().substring(input.getEndInput().length());
            }
            renderer.drawFont(50 + inputWidth, (height - lineHeight * 2), remainingLetters, Color.gray);

        }

        // draw colon?
        renderer.drawFont(30, (height - lineHeight * 2), ">", baseFontColor);

    }

    public cs_Command findCommand(String path){

        String[] pathItems = path.split(" ");

        cs_Package packageLevel = findPackageLevel(path);
        cs_Command command = null;

        int packageIndex = -1;
        if (packageLevel != null) {
            packageIndex = Arrays.asList(pathItems).indexOf(packageLevel.getName().toUpperCase());
        }

        if (packageIndex+1 == pathItems.length) return null;

        ArrayList<cs_Command> commands = new ArrayList<cs_Command>();

        if (packageIndex > -1) {
            commands.addAll(packageLevel.getCommands());
        } else {
            commands.addAll(findCoreCommands());
        }

        // need to adjust package ArrayList like commands to support subpackages

        String search = pathItems[packageIndex+1];

        for(int c = 0; c < commands.size(); c++){

            if (commands.get(c).getName().toUpperCase().equals(search.toUpperCase())) {
                command = commands.get(c);
                break;
            }

        }

        return command;

    }

    public String guessInput(String path) {

        String[] pathItems = path.split(" ");

        cs_Package packageLevel = findPackageLevel(path);

        int packageIndex = -1;
        if (packageLevel != null) {
            packageIndex = Arrays.asList(pathItems).indexOf(packageLevel.getName().toUpperCase());
        }

        ArrayList<cs_Command> commands = new ArrayList<cs_Command>();

        if (packageIndex > -1) {
            commands.addAll(packageLevel.getCommands());
        } else {
            commands.addAll(findCoreCommands());
        }

        // 0 or the end, make suggestion
        if (packageIndex+1 == pathItems.length) {
            if (path.lastIndexOf(" ") + 1 == path.length()) {
                return commands.get(0).getName();
            } else {
                return "";
            }
        } else if(pathItems.length - (packageIndex+1) > 1) {
            return "";
        }

        // need to adjust package ArrayList like commands to support subpackages

        String search = pathItems[packageIndex+1];

        String found = "";

        // look for commands, either core or at packageLevel
        for(int c = 0; c < commands.size(); c++){

            if (commands.get(c).getName().toUpperCase().startsWith(search.toUpperCase())) {
                found = commands.get(c).getName();
                break;
            }

        }

        if (found.equals("") && packageLevel == null) {

            for(int p = 0; p < packages.size(); p++) {

                if (packages.get(p).getName().toUpperCase().toUpperCase().startsWith(search)) {
                    found = packages.get(p).getName();
                }

            }

        }

        return found;

    }

    public cs_Package findPackageLevel(String path){

        String[] pathItems = path.split(" ");

        cs_Package packageLevel = null;

        for(int i = 0; i < pathItems.length; i++){
            boolean isPackage = false;

            for(int p = 0; p < packages.size(); p++){
                System.out.println("CHECK PACKAGE "+packages.get(p).getName().toUpperCase());
                if (packages.get(p).getName().toUpperCase().equals(pathItems[i].toUpperCase())) {
                    packageLevel = packages.get(p);
                    System.out.println("GUESSED PACKAGE " + packages.get(p).getName());
                    isPackage = true;
                    break;
                }
            }

            // break out of loop if no package
            if (isPackage == false) break;

        }

        return packageLevel;
    }

    private ArrayList<cs_Command> findCoreCommands(){

        ArrayList<cs_Command> coreCommands = new ArrayList<cs_Command>();

        for(int p = 0; p < packages.size(); p++){

            if(packages.get(p).isCore())
                coreCommands.addAll(packages.get(p).getCommands());

        }

        return coreCommands;

    }

    public void executeCommand(cs_Command command, String value){

        command.execute(this, value);

    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void toggle(){
        active = !active;
    }

    public void addPackage(cs_Package Package){
        packages.add(Package);
    }

    public String sayString(String string){
        return "You said: "+string;
    }

    public void log(String line){
        input.addLine(line);
    }

    public void log(cs_Line line){
        input.addLine(line);
    }

    public void log(String[] lines){
        for (int l=0; l<lines.length; l++){
            input.addLine(lines[l]);
        }
    }

    public void log(ArrayList<cs_Line> lines){
        for (int l=0; l<lines.size(); l++){
            input.addLine(lines.get(l));
        }
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Color getBaseFontColor() {
        return baseFontColor;
    }

    public void setBaseFontColor(Color baseFontColor) {
        this.baseFontColor = baseFontColor;
    }
}

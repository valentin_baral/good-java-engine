package good.engine.moduleManager.modules.console;

import good.engine.input.gd_Input;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;

import java.util.ArrayList;

//import org.lwjgl.input.Keyboard;

/**
 * Created by valentinbaral on 25/10/2015.
 */
public class cs_Input {

    private cs_Console Console;

    private String currentInput = "";

    private String endInput = "";

    private String guessedInput = "";

    private ArrayList<cs_Line> history = new ArrayList<>();

    public cs_Input(cs_Console Console){

        System.out.println("Input Initiated");
        this.Console = Console;

    }

    public void takeInput(gd_Input input){

        String keyName = Keyboard.getKeyName(Keyboard.getEventKey());

        String keyOut = "";

        if(keyName.length() > 1){

            if(Keyboard.getEventKey() == Keyboard.KEY_SPACE){
                keyOut = " ";
            }else if(Keyboard.getEventKey() == Keyboard.KEY_PERIOD) {
                keyOut = ".";
            }else if(Keyboard.getEventKey() == Keyboard.KEY_ESCAPE) {
                currentInput = "";
            }else if (Keyboard.getEventKey() == Keyboard.KEY_TAB) {
                if (getEndInput().length() < getGuessedInput().length()) {
                    currentInput += getGuessedInput().toUpperCase().substring(getEndInput().length());
                } else {
                    currentInput += " ";
                }
            }else if(Keyboard.getEventKey() == Keyboard.KEY_BACK){

                if(currentInput.length() < 1) return;

                currentInput = currentInput.substring(0,currentInput.length()-1);

                guessLastCommand();
                return;

            }else if(Keyboard.getEventKey() == Keyboard.KEY_RETURN){

                if(currentInput.length() < 1) return;

                addLine("# "+currentInput);

                cs_Command foundCommand = Console.findCommand(currentInput);

                if(foundCommand != null){

                    /*for(int s = index+1; s < pathItems.length; s++){

                        if(s != index+1) value += " ";
                        value += pathItems[s];

                    }*/

                    String value = "";

                    if (endInput != foundCommand.getName()) {
                        value = endInput;
                    }

                    Console.executeCommand(foundCommand, value);

                } else {
                    addLine("Command not found", Color.red);
                }

                currentInput = "";
                guessedInput = "";

                return;

            }

        }else{
            keyOut = keyName;
        }

        currentInput += keyOut;

        guessLastCommand();

    }

    public void guessLastCommand(){

        if (currentInput.length() == 0) {
            guessedInput = "";
            return;
        }

        int lastSpace = currentInput.lastIndexOf(" ");

        if (lastSpace > -1) {
            // should be like lastPathElement or something

            if (lastSpace+1 != currentInput.length()) {
                endInput = currentInput.substring(lastSpace + 1, currentInput.length());
            } else {
                endInput = "";
            }
        } else {
            endInput = currentInput;
        }

        System.out.println("endInput = "+endInput);
        System.out.println("lastSpace = "+lastSpace);

        guessedInput = Console.guessInput(currentInput);

    }

    public String getCurrentInput() {
        return currentInput;
    }

    public ArrayList<cs_Line> getHistory() {
        return history;
    }

    public void addLine(String line){
        addLine(new cs_Line(line, Color.white));
    }

    public void addLine(String line, Color color){
        addLine(new cs_Line(line, color));
    }

    public void addLine(cs_Line line){
        history.add(line);
    }

    public String getGuessedInput() {
        return guessedInput;
    }

    public String getEndInput() {
        return endInput;
    }
}

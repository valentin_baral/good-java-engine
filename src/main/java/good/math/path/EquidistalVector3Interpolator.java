package good.math.path;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector3;

/**
 * Interpolate between Vector3s. The interpolation
 * is done relative to the position of the vector in
 * the array. The distance between each vector is
 * assumed to be equidistal.
 */
public class EquidistalVector3Interpolator extends Path<Vector3> {

    Vector3[] positions;

    private float timePerNode;

    public EquidistalVector3Interpolator(Vector3[] positions) {
        if(positions.length < 2) throw new UnsupportedOperationException("Positions length must be greater that 1");
        this.positions = positions;
        timePerNode = 1f/(positions.length-1);
    }

    @Override
    public Vector3 getAtTime(float t) {
        final int index = Math.min(positions.length-1,(int)(t/timePerNode)+1);
        final int lastIndex = Math.max(0, index - 1);
        final float timeOfLastNode = lastIndex * timePerNode;
        final float remainingTime = t - timeOfLastNode;
        if(remainingTime < 0)
        {
            System.out.println("hh");
        }
        final float alpha = remainingTime / timePerNode;
        System.out.println(alpha);
        return new Vector3(positions[lastIndex]).interpolate(new Vector3(positions[index]), alpha, Interpolation.linear);
    }
}

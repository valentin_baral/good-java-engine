package good.engine.moduleManager.modules.soundGenerator;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Valentin on 06/08/2015.
 */
public class sg_lottery {

    private ArrayList<sg_channelType> tickets = new ArrayList<sg_channelType>();
    private ArrayList<sg_channelType> excludeNext = new ArrayList<sg_channelType>();

    private Random rand = new Random();

    public sg_lottery(){

    }

    public void addTickets(sg_channelType type, int count){

        for(int i = 0; i < count; i++){
            tickets.add(type);
        }

    }

    public void exclude(sg_channelType type){

        excludeNext.add(type);

    }

    public sg_channelType pull(){

        for(int i = 0; i< excludeNext.size(); i++) {
            removeTicketsOfType(excludeNext.get(i));
        }

        excludeNext.clear();

        if(tickets.size() < 1){
            //tickets.add(sg_component.types.LINE);
            return null;
        }

        sg_channelType ticket = tickets.get(rand.nextInt(tickets.size()));

        tickets.clear();

        return ticket;

    }

    public void removeTicketsOfType(sg_channelType type){

        for(int i = tickets.size()-1; i > 0; i--) {
            if(tickets.get(i) == type){

                tickets.remove(i);

            }
        }

    }



}

package good.engine.moduleManager.modules.console;

import org.newdawn.slick.Color;

/**
 * Created by valentinbaral on 27/10/2015.
 */
public class cs_Line {

    private String text;
    private Color color;

    public cs_Line(String text, Color color){
        this.text = text;
        this.color = color;
    }

    public String getText() {
        return text;
    }

    public Color getColor() {
        return color;
    }
}

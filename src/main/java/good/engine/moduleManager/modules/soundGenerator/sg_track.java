package good.engine.moduleManager.modules.soundGenerator;

import java.util.ArrayList;

/**
 * Created by Valentin on 05/08/2015.
 */
public class sg_track {

    private ArrayList<sg_channel> channels = new ArrayList<sg_channel>();


    public sg_track(){}

    public void update(){

        for(int i = 0; i < channels.size(); i++){

            channels.get(i).update();

        }

    }

    public void play(sg_soundGenerator generator){

        for(int i = 0; i < channels.size(); i++){

            //channels.get(i).play(this);

        }

    }

    public void addChannel(sg_channel channel){

        channels.add(channel);

    }

}

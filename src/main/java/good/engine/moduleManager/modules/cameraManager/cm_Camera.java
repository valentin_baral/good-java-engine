package good.engine.moduleManager.modules.cameraManager;

import com.badlogic.gdx.math.Vector3;
import good.math.gd_Math;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

/**
 * Created by Valentin on 13/11/2015.
 */
public class cm_Camera {

    private boolean mouseControlled = false;

    private Vector3f position = new Vector3f();
    private Vector3f rotation = new Vector3f();

    private float fieldOfView = 90;

    private int id = -1;

    public cm_Camera(){

        position.x = 0;
        position.y = 0;
        position.z = 0;

    }

    public float getFieldOfView() {
        return fieldOfView;
    }

    public void setFieldOfView(float fieldOfView) {
        this.fieldOfView = fieldOfView;
    }

    public Vector3f getPosition() {
        return position;
    }
    public Vector3 getPosition2() {
        return gd_Math.convertVector(position);
    }
    public Vector3f getRotation() {
        return rotation;
    }

    public void setRotation(Vector3f rotation) {
        setRotation(rotation.x,rotation.y, rotation.z);;
    }
    public void setRotation(Vector3 rotation) {
        setRotation(rotation.x,rotation.y, rotation.z);;
    }

    public void setRotation(float x, float y, float z) {
        rotation.set(x,y,z);
    }

    public void setPosition(Vector3 position) {
        setPosition(position.x,position.y, position.z);
    }
    public void setPosition(Vector3f position) {
        setPosition(position.x,position.y, position.z);
    }

    public void setPosition(float x, float y, float z) {
        position.set(x,y,z);
    }


    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public boolean isMouseControlled() {
        return mouseControlled;
    }

    public void setMouseControlled(boolean mouseControlled) {
        this.mouseControlled = mouseControlled;
    }

}

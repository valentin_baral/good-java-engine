package good.engine.entity.graphics;

/**
 * Created by valentinbaral on 11/11/2017.
 */
public class gd_gfx_Texture {

    private String url;
    private int texture;
    private float textureWidth;
    private float textureHeight;
    private boolean repeatTexture;

    public gd_gfx_Texture(String url, int texture, float textureWidth, float textureHeight, boolean repeatTexture) {
        this.url = url;
        this.texture = texture;
        this.textureWidth = textureWidth;
        this.textureHeight = textureHeight;
        this.repeatTexture = repeatTexture;
    }

    public int getTexture() {
        return texture;
    }

    public float getTextureWidth() {
        return textureWidth;
    }

    public float getTextureHeight() {
        return textureHeight;
    }

    public boolean isRepeatTexture() {
        return repeatTexture;
    }

    public String getUrl() {
        return url;
    }
}

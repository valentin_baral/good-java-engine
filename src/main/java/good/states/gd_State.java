package good.states;

import good.engine.input.gd_Input;
import good.engine.main.gd_Build;
import good.engine.output.render.gd_output_Renderer;

import javax.inject.Inject;

/**
 * Created by valentinbaral on 17/11/2015.
 */
public class gd_State {

    @Inject
    public gd_State(){

    }

    public void init(gd_Build engine){};

    public void takeInput(gd_Input input){}
    public void takePolledInput(gd_Input input){}

    public void preUpdate(gd_Build engine){}
    public void update(gd_Build engine){}
    public void postUpdate(gd_Build engine){}

    public void preDraw(gd_output_Renderer renderer){}
    public void draw(gd_output_Renderer renderer){}
    public void postDraw(gd_output_Renderer renderer){}

    public void drawHud(gd_output_Renderer renderer){}

}

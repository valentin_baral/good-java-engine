package good.engine.moduleManager.modules.soundGenerator;

/**
 * Created by Valentin on 05/08/2015.
 */
public class sg_instrument {

    private float pinch;
    private float[] mod;

    public sg_instrument(float pinch, float[] mod){

        if(pinch >= -1f && pinch <= 1f) this.pinch = pinch;
        else this.pinch = 0;

        this.mod = mod;

    }

    public float getPinch() {
        return pinch;
    }

    public float[] getMod() {
        return mod;
    }
}

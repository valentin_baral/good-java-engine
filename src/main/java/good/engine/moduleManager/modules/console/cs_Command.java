package good.engine.moduleManager.modules.console;

import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * Created by valentinbaral on 24/10/2015.
 */
public class cs_Command {

    private String name;
    private String target;
    private boolean hasValue;

    private cs_Package Package;

    public cs_Command(String name, String target, boolean hasValue){
        this.name = name;
        this.target = target;
        this.hasValue = hasValue;
    }

    public void execute(cs_Console Console, String value){

        System.out.println("execute, value is: "+value);

        String type = Package.getType();

        Object ins = Package.getReference();

        ins.getClass();

        Class<?> realClass = ins.getClass();

        realClass.cast(ins);

        Object response = null;

        try {

            if(hasValue) {

                Class[] argTypes = new Class[]{String.class};

                Method func = realClass.getDeclaredMethod(target, argTypes);
                response = func.invoke(ins, value);

            }else{

                Method func = realClass.getDeclaredMethod(target);
                response = func.invoke(ins);

            }

        } catch (Exception e) {
            e.printStackTrace();
            response = "Error";
        }

        if(response != null){



            if(response instanceof String) {

                Console.log((String) response);

            }else if(response instanceof String[]){

                String[] lines = (String[]) response;

                for(int l = 0; l < lines.length; l++){
                    Console.log(lines[l]);
                }

            }else if(response instanceof ArrayList){

                ArrayList<Object> lines = (ArrayList) response;

                for(int l = 0; l < lines.size(); l++){
                    Object line = lines.get(l);

                    if (line instanceof cs_Line) {
                        Console.log((cs_Line)line);
                    }
                }

            }
        }

    }

    public String getName() {
        return name;
    }

    public String getTarget() {
        return target;
    }

    public boolean hasValue() {
        return hasValue;
    }

    public void setPackage(cs_Package Package){
        this.Package = Package;
    }

}

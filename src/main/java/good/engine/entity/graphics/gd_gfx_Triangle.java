package good.engine.entity.graphics;

import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;

/**
 * Created by valentinbaral on 15/11/2015.
 */
public class gd_gfx_Triangle {

    private ArrayList<Vector3f> points = new ArrayList<Vector3f>();

    private Vector3f normals = new Vector3f(0, 0, 0);

    public gd_gfx_Triangle(){}

    public gd_gfx_Triangle(Vector3f p1, Vector3f p2, Vector3f p3, Vector3f normals ){

        this.normals.set(normals);

        points.add(p1);
        points.add(p2);
        points.add(p3);

    }

    public ArrayList<Vector3f> getPoints() {
        return points;
    }

    public Vector3f getNormals() {
        return normals;
    }

}

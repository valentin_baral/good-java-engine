package good.engine.output.render;

import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

/**
 * Created by Valentin on 27/03/2016.
 */
public class gd_output_Scene {

    private Vector3f sky = new Vector3f(0,0,0);
    private float fogStart = 450;
    private float fogEnd = 500;

    private Vector4f lightDiffuse = new Vector4f(0.5f,0.5f,0.5f,1f);
    private Vector4f lightSpecular = new Vector4f(0.5f,0.5f,0.5f,1f);
    private Vector4f lightAmbient = new Vector4f(0.5f,0.5f,0.5f,1f);

    public gd_output_Scene() {

    }

    public Vector3f getSky() {
        return sky;
    }

    public void setSky(Vector3f sky) {
        this.sky = sky;
    }

    public float getFogStart() {
        return fogStart;
    }

    public void setFogStart(float fogStart) {
        this.fogStart = fogStart;
    }

    public float getFogEnd() {
        return fogEnd;
    }

    public void setFogEnd(float fogEnd) {
        this.fogEnd = fogEnd;
    }

    public Vector4f getLightDiffuse() {
        return lightDiffuse;
    }

    public void setLightDiffuse(Vector4f lightDiffuse) {
        this.lightDiffuse.set(lightDiffuse);
    }

    public Vector4f getLightSpecular() {
        return lightSpecular;
    }

    public void setLightSpecular(Vector4f lightSpecular) {
        this.lightSpecular.set(lightSpecular);
    }

    public Vector4f getLightAmbient() {
        return lightAmbient;
    }

    public void setLightAmbient(Vector4f lightAmbient) {
        this.lightAmbient.set(lightAmbient);
    }
}

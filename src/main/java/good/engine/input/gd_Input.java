package good.engine.input;

import good.engine.main.gd_Build;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import javax.inject.Inject;


/**
 * Created by valentinbaral on 02/11/2015.
 */
public class gd_Input {

    int dWheel = 0;

    public gd_Input(){

        // set these to variables
        Mouse.setGrabbed(false);
        Mouse.setClipMouseCoordinatesToWindow(false);
        Keyboard.enableRepeatEvents(true);

    }

    public void takeInput(gd_Build engine){

        if (Mouse.hasWheel()) {
            dWheel = Mouse.getDWheel();
        }

        while (Keyboard.next()) {
            engine.takePolledInput(this);
        }

        engine.takeInput(this);

    }

    public int getdWheel() {
        return dWheel;
    }

}

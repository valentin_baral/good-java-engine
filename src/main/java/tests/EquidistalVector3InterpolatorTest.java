package tests;

import com.badlogic.gdx.math.Vector3;
import good.math.path.EquidistalVector3Interpolator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EquidistalVector3InterpolatorTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getAtTime() {
        EquidistalVector3Interpolator path = new EquidistalVector3Interpolator(new Vector3[]{new Vector3(0,0,0), new Vector3(0,50,0), new Vector3(0,0,0)});
        assertEquals(new Vector3(0 ,0,0), path.getAtTime(0), "Position at 0 failed");
        assertEquals(new Vector3(0,0,0), path.getAtTime(1), "Position at 1 failed");
        assertEquals(new Vector3(0,50,0), path.getAtTime(0.5f), "Position at 0.5 failed");
        assertEquals(0, new Vector3(0,0,0).sub(path.getAtTime(0.000000001f)).y, 0.1, "Position at 0.0000 failed");

    }
}
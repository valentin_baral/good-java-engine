package good.states;

import com.sun.javaws.exceptions.InvalidArgumentException;
import good.engine.input.gd_Input;
import good.engine.main.gd_Build;
import good.engine.output.render.gd_output_Renderer;

import javax.inject.Inject;
import java.util.ArrayList;

/**
 * Created by valentinbaral on 17/11/2015.
 */
public class gd_StateManager {

    private ArrayList<gd_State> states = new ArrayList<gd_State>();

    private gd_State currentState = null;

    @Inject
    public gd_StateManager(){

    }

    public void init(gd_Build engine) {
        if(currentState == null) throw new IllegalArgumentException("No state set!");
        currentState.init(engine);
    }

    public void takeInput(gd_Input input) {

        if(currentState != null)
            currentState.takeInput(input);

    }

    public void takePolledInput(gd_Input input) {

        if(currentState != null)
            currentState.takePolledInput(input);

    }

    public void preUpdate(gd_Build engine) {

        if(currentState != null)
            currentState.preUpdate(engine);

    }

    public void update(gd_Build engine) {

        if(currentState != null)
            currentState.update(engine);

    }

    public void postUpdate(gd_Build engine) {

        if(currentState != null)
            currentState.postUpdate(engine);

    }

    public void preDraw(gd_output_Renderer renderer) {

        if(currentState != null)
            currentState.preDraw(renderer);

    }

    public void draw(gd_output_Renderer renderer) {

        if(currentState != null)
            currentState.draw(renderer);

    }

    public void postDraw(gd_output_Renderer renderer) {

        if(currentState != null)
            currentState.postDraw(renderer);

    }

    public void drawHud(gd_output_Renderer renderer) {

        if(currentState != null)
            currentState.drawHud(renderer);

    }


    public gd_State getCurrentState() {
        return currentState;
    }

    public void setCurrentState(gd_State currentState) {
        this.currentState = currentState;
    }
}

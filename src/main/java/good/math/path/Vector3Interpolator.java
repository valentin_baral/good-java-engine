package good.math.path;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector3;

/**
 * Interpolate between Vector3s. The interpolation
 * is done relative to the absolute distance between
 * the set positions.
 */
public class Vector3Interpolator extends Path<Vector3> {

    Vector3[] positions;
    private float absoluteDistanceSquared = 0;

    public Vector3Interpolator(Vector3[] positions) {
        if(positions.length < 2) throw new UnsupportedOperationException("Positions length must be greater that 1");
        this.positions = positions;
        for(int i = 1; i < positions.length; i++)
        {
            final Vector3 prevPosition = new Vector3(positions[i-1]);
            final Vector3 position = new Vector3(positions[i]);
            absoluteDistanceSquared += new Vector3(position).sub(prevPosition).len();
        }
    }

    @Override
    public Vector3 getAtTime(float t) {
        float currentAbsDistanceSquared = 0;
        for(int i = 1; i < positions.length; i++)
        {
            final Vector3 prevPosition = new Vector3(positions[i-1]);
            final Vector3 position = new Vector3(positions[i]);
            final Vector3 distance = new Vector3(prevPosition).sub(position);
            final float currentDistance = distance.len();
            final float currentDistanceSquared = currentAbsDistanceSquared + currentDistance;
            final float currentTime = currentDistanceSquared / absoluteDistanceSquared;
            if(currentTime > t)
            {
                final float prevTime = currentAbsDistanceSquared / absoluteDistanceSquared;
                final float endPieceTime = t - prevTime;
                final float distanceLeft = endPieceTime * absoluteDistanceSquared;
                final Vector3 interpolated = new Vector3(prevPosition).interpolate(position, distanceLeft/currentDistance, Interpolation.linear);
                return interpolated;
            }
            else
            {
                currentAbsDistanceSquared = currentDistanceSquared;
            }
        }
        return positions[positions.length-1];
    }
}



package good.engine.main;

import good.states.gd_State;

import static java.lang.Thread.currentThread;

/**
 * Created by valentinbaral on 02/11/2015.
 */
public class gd_Main {

    public static void main(String[] args) {
        //gd_Core engine = new gd_Core();
        new gd_Main().init();
    }

    protected gd_Core engine;
    protected gd_State game;
    public gd_Main()
    {
        System.out.println("gdMain "+currentThread().getId());

        engine = new gd_Core();
        init();
    }

    public void init()
    {
        if(game == null) throw new IllegalArgumentException("You need to set a game to start the engine");
        engine.start(game);
    }


}

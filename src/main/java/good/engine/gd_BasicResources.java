package good.engine;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import good.engine.entity.graphics.*;
import good.engine.entity.hitbox.gd_Hitbox;
import good.engine.entity.objects.gd_Object;
import good.math.gd_Math;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.opengl.PNGDecoder;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import javax.inject.Inject;
import javax.inject.Singleton;

import java.util.ArrayList;

import static org.lwjgl.opengl.GL11.GL_NEAREST;

@Singleton
public class gd_BasicResources {

    gd_gfx_Shape cube;
    gd_gfx_Shape quad;

    private ArrayList<gd_gfx_Texture> textures = new ArrayList<>();

    @Inject
    public gd_BasicResources() {
    }

    public gd_gfx_BillboardShape buildBillboard(Vector2 scale) {
        return buildBillboard(new Vector3f((float)scale.x, (float)scale.y, 0.1f));
    }

    public gd_gfx_BillboardShape buildBillboard(Vector3f scale){

        gd_gfx_BillboardShape shape = new gd_gfx_BillboardShape();
        cube.clone(shape);
        shape.setBillboard(true);
        shape.setScale(scale);
        return shape;

    }

    public gd_gfx_Shape buildBoxShape(Vector3f SIZE){
        gd_gfx_Shape newBox = getCube().clone();
        newBox.setScale(SIZE);
        return newBox;
    }
    public gd_gfx_Shape buildQuadShape(Vector2 SIZE){
        gd_gfx_Shape newBox = getQuad().clone();
        newBox.setScale(new Vector3f(SIZE.x, SIZE.y, 0));
        return newBox;
    }

    private gd_gfx_Shape getQuad() {
        if (quad == null) {
            quad = createQuad();
        }
        return quad;
    }
    private gd_gfx_Shape getCube() {
        if (cube == null) {
            cube = createCube();
        }
        return cube;
    }

    public gd_Object buildBoxObject(Vector3 size){
        return buildBoxObject(gd_Math.convertVector(size));
    }
    public gd_Object buildBoxObject(Vector3f size){

        gd_Object box = new gd_Object();

        box.addHitbox(new gd_Hitbox(new Vector3f(size.x, size.y, size.z)));

        gd_gfx_Model model = new gd_gfx_Model();

        gd_gfx_Shape shape = buildBoxShape(new Vector3f(size.x, size.y, size.z));
        //shape.setTexture(resources.wood);
        shape.setColor(55,55,55);
        model.addShape(shape);
        box.setModel(model);
        return box;
    }

    public gd_gfx_Shape createCube(){

        gd_gfx_Shape shape = new gd_gfx_Shape();

        shape.red = 255;
        shape.green = 0;
        shape.blue = 0;

        float size = 1f;

        // TOP & BOTTOM

        // top
        shape.getTriangles().add(new gd_gfx_Triangle(new Vector3f(-size / 2, size / 2, -size / 2), new Vector3f(-size / 2, size / 2, size / 2), new Vector3f(size / 2, size / 2, -size / 2), new Vector3f(0, 1, 0)));
        shape.getTriangles().add(new gd_gfx_Triangle(new Vector3f(-size / 2, size / 2, size / 2), new Vector3f(size / 2, size / 2, size / 2), new Vector3f(size / 2, size / 2, -size / 2), new Vector3f(0,1,0)));

        // bottom
        shape.getTriangles().add(new gd_gfx_Triangle(new Vector3f(-size / 2, -size / 2, size / 2), new Vector3f(-size / 2, -size / 2, -size / 2), new Vector3f(size / 2, -size / 2, -size / 2), new Vector3f(0, -1, 0)));
        shape.getTriangles().add(new gd_gfx_Triangle(new Vector3f(-size / 2, -size / 2, size / 2), new Vector3f(size / 2, -size / 2, -size / 2), new Vector3f(size / 2, -size / 2, size / 2), new Vector3f(0, -1, 0)));

        // LEFT & RIGHT

        // left
        shape.getTriangles().add(new gd_gfx_Triangle(new Vector3f(-size / 2, -size / 2, -size / 2), new Vector3f(-size / 2, -size / 2, size / 2), new Vector3f(-size / 2, size / 2, size / 2), new Vector3f(-1, 0, 0)));
        shape.getTriangles().add(new gd_gfx_Triangle(new Vector3f(-size / 2, -size / 2, -size / 2), new Vector3f(-size / 2, size / 2, size / 2), new Vector3f(-size / 2, size / 2, -size / 2), new Vector3f(-1, 0, 0)));

        // right rw? => ????
        shape.getTriangles().add(new gd_gfx_Triangle(new Vector3f(size / 2, -size / 2, -size / 2), new Vector3f(size / 2, size / 2, size / 2), new Vector3f(size / 2, -size / 2, size / 2), new Vector3f(1, 0, 0)));
        shape.getTriangles().add(new gd_gfx_Triangle(new Vector3f(size / 2, -size / 2, -size / 2), new Vector3f(size / 2, size / 2, -size / 2), new Vector3f(size / 2, size / 2, size / 2), new Vector3f(1, 0, 0)));


        // BACK & FRONT

        // back
        shape.getTriangles().add(new gd_gfx_Triangle(new Vector3f(size / 2, -size / 2, -size / 2), new Vector3f(-size / 2, -size / 2, -size / 2), new Vector3f(-size / 2, size / 2, -size / 2), new Vector3f(0, 0, -1)));
        shape.getTriangles().add(new gd_gfx_Triangle(new Vector3f(size / 2, -size / 2, -size / 2), new Vector3f(-size / 2, size / 2, -size / 2), new Vector3f(size / 2, size / 2, -size / 2), new Vector3f(0, 0, -1)));

        // front
        shape.getTriangles().add(new gd_gfx_Triangle(new Vector3f(size / 2, -size / 2, size / 2), new Vector3f(-size / 2, size / 2, size / 2), new Vector3f(-size / 2, -size / 2, size / 2), new Vector3f(0, 0, 1)));
        shape.getTriangles().add(new gd_gfx_Triangle(new Vector3f(size / 2, -size / 2, size / 2), new Vector3f(size / 2, size / 2, size / 2), new Vector3f(-size / 2, size / 2, size / 2), new Vector3f(0, 0, 1)));

        return shape;

    }

    public gd_gfx_Shape createQuad(){

        gd_gfx_Shape shape = new gd_gfx_Shape();

        shape.red = 255;
        shape.green = 0;
        shape.blue = 0;

        float size = 1f;

        shape.getTriangles().add(new gd_gfx_Triangle(new Vector3f(size / 2, -size / 2, 0), new Vector3f(-size / 2, size / 2, 0), new Vector3f(-size / 2, -size / 2, 0), new Vector3f(0, 0, 1)));
        shape.getTriangles().add(new gd_gfx_Triangle(new Vector3f(size / 2, -size / 2, 0), new Vector3f(size / 2, size / 2, 0), new Vector3f(-size / 2, size / 2, 0), new Vector3f(0, 0, 1)));

        return shape;
    }

    public PNGDecoder getTextureByteArray(String url) throws Exception
    {
        return new PNGDecoder(ResourceLoader.getResourceAsStream(url));
    }
    public gd_gfx_Texture loadTexture(String url, float textureWidth, float textureHeight, boolean repeatTexture) {
        return loadTexture(url,textureWidth,textureHeight,repeatTexture,false);
    }
    public gd_gfx_Texture loadTexture(String url, float textureWidth, float textureHeight, boolean repeatTexture, boolean silent) {

        Texture texLoad = null;
        gd_gfx_Texture texture = getTextureForUrl(url);

        if (texture != null) {
            return texture;
        }

        try {
            // load texture from PNG file
            texLoad = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(url), GL_NEAREST);

            System.out.println("Load texture: "+url);
            texture = new gd_gfx_Texture(url, texLoad.getTextureID(), textureWidth, textureHeight, repeatTexture);

            /*System.out.println("Texture loaded: "+texture);
            System.out.println(">> Image width: "+texture.getImageWidth());
            System.out.println(">> Image height: "+texture.getImageHeight());
            System.out.println(">> Texture width: "+texture.getTextureWidth());
            System.out.println(">> Texture height: "+texture.getTextureHeight());
            System.out.println(">> Texture ID: "+texture.getTextureID());*/

        } catch (Exception e) {
            if(!silent) e.printStackTrace();
            return null;
        }
        if (texture != null) {
            textures.add(texture);
        }

        return texture;

    }

    public gd_gfx_Texture getTextureForUrl(String url) {
        for (int t=0; t<textures.size(); t++) {
            if (textures.get(t).getUrl().equals(url)) {
                return textures.get(t);
            }
        }
        return null;
    }
}

package good.engine.output.render;

import org.lwjgl.util.vector.Vector3f;

/**
 * Created by Valentin on 04/12/2015.
 */
public class gd_output_Camera {

    private Vector3f position = new Vector3f();
    private Vector3f rotation = new Vector3f();

    private float fieldOfView = 90;

    private int id = -1;

    public gd_output_Camera(){

        position.x = 0;
        position.y = 0;
        position.z = 0;

    }

    public float getFieldOfView() {
        return fieldOfView;
    }

    public void setFieldOfView(float fieldOfView) {
        this.fieldOfView = fieldOfView;
    }

    public Vector3f getPosition() {
        return position;
    }
    public Vector3f getRotation() {
        return rotation;
    }

    public void setRotation(Vector3f rotation) {
        this.rotation.set(rotation);
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

}

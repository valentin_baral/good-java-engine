package good.engine.entity.objects;

import good.engine.helper.collision.gd_Collision;
import good.engine.input.gd_Input;
import good.engine.main.gd_Build;
import good.engine.output.render.gd_output_Renderer;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by valentinbaral on 18/11/2015.
 */
@Singleton
public class gd_ObjectManager {

    private ArrayList<gd_Object> allObjects = new ArrayList<gd_Object>();
    private ArrayList<gd_Object> objects = new ArrayList<gd_Object>();

    @Inject
    public gd_ObjectManager(){

        System.out.println("objectManager initiated");

    }

    public void preUpdate(gd_Build engine){

        for(int o = 0, os = objects.size(); o < os; o++){
            if(objects.get(o).isActive() && objects.get(o).isActive2())
                objects.get(o).preUpdate(engine);
        }

    }

    public void update(gd_Build engine){

        // animation
        for(int o = 0, os = objects.size(); o < os; o++){

            gd_Object modObject = objects.get(o);
            if(!modObject.isActive() || !objects.get(o).isActive2()) continue;
            if(modObject.getModel() != null) modObject.getModel().updateAnimations();

        }

        // update
        for(int o = 0, os = objects.size(); o < os; o++){
            if(objects.get(o).isActive() && objects.get(o).isActive2())
                objects.get(o).update(engine);
        }

    }

    public void postUpdate(gd_Build engine){

        // to avoid flooding of inactive objects
        objects.clear();

        for(int o = 0, os = allObjects.size(); o < os; o++){
            if(allObjects.get(o).isActive() && allObjects.get(o).isActive2())
                objects.add(allObjects.get(o));
        }

        for(int o = 0, os = objects.size(); o < os; o++){
            if(objects.get(o).isActive() && objects.get(o).isActive2())
                objects.get(o).velocity();
        }

        for(int o = 0, os = objects.size(); o < os; o++){
            if(objects.get(o).isActive() && objects.get(o).isActive2())
                objects.get(o).gravity();
        }

        for(int o = 0, os = objects.size(); o < os; o++){
            if(objects.get(o).isActive() && objects.get(o).isActive2())
                gd_Collision.check(objects.get(o), objects);
        }

        for(int o = 0, os = objects.size(); o < os; o++){
            if(objects.get(o).isActive() && objects.get(o).isActive2())
                objects.get(o).move();
        }

        for(int o = 0, os = objects.size(); o < os; o++){
            if(objects.get(o).isActive() && objects.get(o).isActive2())
                objects.get(o).postUpdate(engine);
        }

    }

    public void preDraw(gd_output_Renderer renderer){

        for(int o = 0, os = objects.size(); o < os; o++){
            if(objects.get(o).isActive() && objects.get(o).isActive2())
                objects.get(o).preDraw(renderer);
        }

    }

    public void draw(gd_output_Renderer renderer){
        for(int o = 0, os = objects.size(); o < os; o++){

            gd_Object object = objects.get(o);
            if(!object.isActive() || !objects.get(o).isActive2()) continue;
            if(object.getModel() == null) continue;
            if(!object.getModel().isVisible()) continue;
            renderer.drawModel(object.getPosition(), object.getModel());

        }

        for(int o = 0, os = objects.size(); o < os; o++){
            if(objects.get(o).isActive() && objects.get(o).isActive2())
                objects.get(o).draw(renderer);
        }

    }

    public void postDraw(gd_output_Renderer renderer){

        for(int o = 0, os = objects.size(); o < os; o++){
            if(objects.get(o).isActive() && objects.get(o).isActive2())
                objects.get(o).postDraw(renderer);
        }

    }

    public void drawHud(gd_output_Renderer renderer){

        for(int o = 0, os = objects.size(); o < os; o++){
        }

    }

    public void takeInput(gd_Input input){

        for(int o = 0, os = objects.size(); o < os; o++){
            if(objects.get(o).isActive() && objects.get(o).isActive2())
                objects.get(o).takeInput(input);
        }

    }

    public void takePolledInput(gd_Input input){

        for(int o = 0, os = objects.size(); o < os; o++){
            if(objects.get(o).isActive() && objects.get(o).isActive2())
                objects.get(o).takePolledInput(input);
        }

    }

    public void addObject(gd_Object object){
        allObjects.add(object);
        //objects.add(object);

        //orderObjectsForDraw();
    }

    public void addObjects(ArrayList<gd_Object> objects){
        allObjects.addAll(objects);
        //this.objects.addAll(objects);
        //orderObjectsForDraw();
    }

    private void orderObjectsForDraw() {
        Collections.sort(allObjects, new gd_ObjectComparator());
    }

    /*public ArrayList<gd_Object> getObjects(){
        return objects;
    }*/

    public void removeObjects(ArrayList<gd_Object> objects){
        allObjects.removeAll(objects);
    }

    public void removeObject(gd_Object object){
        allObjects.remove(object);
    }

    /**
     * Created by valentinbaral on 03/11/2015.
     */

    public static class gd_ObjectComparator implements Comparator<gd_Object> {
        @Override
        public int compare(gd_Object o1, gd_Object o2) {

            if (o1.getModel() == null) {
                return -1;
            } else if (o1.getModel() == null && o2.getModel() == null) {
                return 0;
            } else if (o1.getModel() != null && o2.getModel() == null) {
                return 1;
            }

            if (!o1.getModel().hasFlatTexture && !o2.getModel().hasFlatTexture) {
                return 0;
            } else if (!o1.getModel().hasFlatTexture && o2.getModel().hasFlatTexture) {
                return -1;
            } else if (o1.getModel().hasFlatTexture && !o2.getModel().hasFlatTexture) {
                return 1;
            } else if (o1.getModel().hasFlatTexture && o2.getModel().hasFlatTexture) {
                if (o1.getPosition().z < o2.getPosition().z){
                    return -1;
                } else if (o1.getPosition().z > o2.getPosition().z) {
                    return 1;
                } /*else {
                    return 0;
                }*/
                else {
                    if (o1.getModel().getRotation().y != 0 && o2.getModel().getRotation().y == 0) {
                        return 1;
                    } else if (o1.getModel().getRotation().y == 0 && o2.getModel().getRotation().y != 0) {
                        return -1;
                    }
                    return 0;
                }

            }

            return 0;

        }
    }

}

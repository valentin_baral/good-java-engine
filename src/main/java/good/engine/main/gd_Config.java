package good.engine.main;

/**
 * Created by valentinbaral on 03/11/2015.
 */
public class gd_Config {

    public static String name = "Game";
    public static int WIDTH = 800;
    public static int HEIGHT = 600;
    public static final int updatesPerSecond = 30;
    public static int maxFps = 300;
    public static float gravity = 75;
    public static boolean wireframe = false;
    public static float sensitivity = 4f;

    public static boolean vSyncEnabled = true;

    public static boolean drawFps = false;

    public static float volume = 1f;


    /**
     * @return seconds
     */
    public static float hardDeltaTime() {
        return 1f/(float) updatesPerSecond;
    }

}

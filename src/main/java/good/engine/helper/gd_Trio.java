package good.engine.helper;

public class gd_Trio<L,M,R> {

    private final L left;
    private final M middle;
    private final R right;

    public gd_Trio(L left, M middle, R right) {
        this.left = left;
        this.middle = middle;
        this.right = right;
    }

    public L getLeft() { return left; }

    public M getMiddle() {
        return middle;
    }

    public R getRight() { return right; }

    @Override
    public int hashCode() { return left.hashCode() ^ right.hashCode(); }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof gd_Trio)) return false;
        gd_Trio trioo = (gd_Trio) o;
        return this.left.equals(trioo.getLeft()) &&
                this.middle.equals(trioo.getMiddle()) &&
                this.right.equals(trioo.getRight());
    }

}